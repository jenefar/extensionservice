package com.piqube.marketplace.controller;

import com.google.gson.Gson;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;
import com.piqube.marketplace.model.ExtensionProfile;
import com.piqube.marketplace.model.Users;
import com.piqube.marketplace.parser.sites.linkedin.LinkedinParser;
import com.piqube.marketplace.parser.sites.naukri.NaukriParser;
import com.piqube.marketplace.service.ExtensionService;
import com.piqube.marketplace.service.ProfileServiceDao;
import com.piqube.marketplace.service.UserService;
import com.piqube.marketplace.util.Helper;
import com.piqube.model.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by jenefar on 20/01/16.
 */
@RestController
@EnableRedisHttpSession
@EntityScan(basePackages="com.piqube")
@EnableJpaRepositories({"com.piqube.marketplace.repository"})

//@RequestMapping("/extension")
public class ExtensionController {


    @Autowired
    ExtensionService extensionService;

    @Autowired
    UserService userservice;


    private static final Logger LOGGER = LoggerFactory.getLogger(ExtensionController.class);

    @RequestMapping(value="/extension/check", method = RequestMethod.GET)
    @ResponseBody
    public String check(@RequestParam(value="url", required=true)String pageUrl) throws MalformedURLException {
        System.out.println("====inside check:" + pageUrl);

        return "SUCCESS";
    }
        @RequestMapping(value="/extension/can-open-extension", method = RequestMethod.GET)
    @ResponseBody
    public String canOpenExtension(@RequestParam(value="url", required=true)String pageUrl) throws MalformedURLException {
        System.out.println("====inside canOpenExtension:"+pageUrl);

        Boolean retVal = Boolean.FALSE;
        if(pageUrl == null) {
            return retVal.toString();
        }
        try {
            System.out.println("====pageurl:"+pageUrl);

            //temp fix
            pageUrl=pageUrl.replaceAll("-","_");

            URL url = new URL(pageUrl);
            String host  = url.getHost();
            String path = url.getPath();
            if(host.matches(".*.linkedin.com") || host.matches(".*linkedin.com")) {
                if(path.matches("/pub/dir/.*")) {
                    retVal = Boolean.FALSE;
                } else if(path.matches("/\\w{2}/\\w{1,30}") || path.matches("/pub/.*") || path.matches("/profile/view.*")) {
                    retVal = Boolean.TRUE;
                }
            } else if (host.matches(".*.github.com") || host.matches(".*github.com")) {
                if(path.matches("/.*/.*")) {
                    retVal = Boolean.FALSE;
                }else if(path.matches("/.*")) {
                    retVal = Boolean.TRUE;
                }
            } else if (host.matches(".*.naukri.com") || host.matches(".*naukri.com")) {
                if(path.matches("/preview/preview.*")) {
                    retVal = Boolean.TRUE;
                }
            } else if (host.matches(".*.monsterindia.com") || host.matches(".*monsterindia.com")) {
                if(path.matches("/v2/resumedatabase/.*")) {
                    retVal = Boolean.TRUE;
                }
            } else if (host.matches(".*.stackoverflow.com") || host.matches(".*stackoverflow.com")) {
                if(path.matches("/users/.*")) {
                    retVal = Boolean.TRUE;
                }
            } else if (host.matches(".*.twitter.com") || host.matches(".*twitter.com")) {
                if(path.matches("/.*/.*")) {
                    retVal = Boolean.FALSE;
                }else if(path.matches("/.*")) {
                    retVal = Boolean.TRUE;
                }
            }

            else if (host.matches(".*.employer.dice.com") || host.matches(".*employer.dice.com")) {
                if(path.matches("/.*")) {
                    LOGGER.debug("true");
                    retVal = Boolean.TRUE;
                }
            }
            else if (host.matches(".*.recruiter.wisdomjobs.com") || host.matches(".*recruiter.wisdomjobs.com")) {
                if(path.matches("/.*")) {
                    retVal = Boolean.TRUE;
                }
            }
            else if (host.matches(".*.jobstreet.co") || host.matches(".*jobstreet.co.*")) {
                LOGGER.debug("inside jobstreet:::::::::::::::::::");
                if(path.matches("/.*employer.*")) {
                    LOGGER.debug("inside jobstreet if");
                    retVal = Boolean.TRUE;
                }
            }
            else if (host.matches(".*.jobsdb.com") || host.matches(".*jobsdb.com")) {
                if(path.matches(".*Employer.*") || path.matches(".*employer.*")) {
                    LOGGER.debug("inside jobsdb.com if");
                    retVal = Boolean.TRUE;
                }
            }
            else if (host.matches(".*.monster.com") || host.matches(".*monster.com")) {
                if(path.matches("/v2/resumedatabase/.*")) {
                    retVal = Boolean.TRUE;
                }
            }
            else if (host.matches(".*.recruit.com") || host.matches(".*recruit.com")) {
                if(path.matches("/.*employer.*")) {
                    retVal = Boolean.TRUE;
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage());
            retVal = Boolean.FALSE;
        }

        return retVal.toString();
    }

    @RequestMapping(value="/extension/get-request-id", method=RequestMethod.GET)
    @ResponseBody
    public String getUUID() {
        String uuid = java.util.UUID.randomUUID().toString();
        return uuid;
    }

    @RequestMapping(value="/extension/{requestId}/upload", method=RequestMethod.POST)
    @ResponseBody
    public String fileUpload(byte[] file, @RequestParam(required = true, value = "url") String url, @PathVariable String requestId, HttpServletRequest request )
            throws Exception {
        LOGGER.debug("File upload 'Post' method...");

        // validate a file was entered
        if (file.length == 0) {
            LOGGER.debug("File content length empty...");
            return Boolean.FALSE.toString();
        }

        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        CommonsMultipartFile multipartFile = (CommonsMultipartFile) multipartRequest.getFile("file");

        // the directory to upload to
        String uploadDir = "/tmp/upload/";

        // Create the directory if it doesn't exist
        File dirPath = new File(uploadDir);

        if (!dirPath.exists()) {
            dirPath.mkdirs();
        }

        //retrieve the file data
        InputStream stream = null;
        //write the file to the file specified
        OutputStream bos = null;
        String outFilePath = null;
        if(multipartFile != null && !multipartFile.isEmpty()) {
            outFilePath = uploadDir + multipartFile.getOriginalFilename();
            stream = multipartFile.getInputStream();
            bos = new FileOutputStream(outFilePath);
        } else {
            outFilePath = uploadDir + requestId;
            stream = new ByteArrayInputStream(file);
            bos = new FileOutputStream(outFilePath);
        }
        int bytesRead;
        byte[] buffer = new byte[8192];

        while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
            bos.write(buffer, 0, bytesRead);
        }

        bos.close();

        //close the stream
        stream.close();


        /*Request uploadRequest = new Request();
        uploadRequest.setRequestId(requestId);
        uploadRequest.setUrl(URLDecoder.decode(url, "UTF-8"));
        uploadRequest.setMd5Url(ConvertUtil.convertToMD5(url));
        uploadRequest.setFilePath(outFilePath);
        requestManager.saveRequest(uploadRequest);*/

        System.out.println("url is::::"+url);
        LOGGER.debug("====filepath is====="+outFilePath);
        String fileContent=readFile(outFilePath);
        Person person = null;
        if(url.contains("linkedin")) {
            LinkedinParser parser = new LinkedinParser();
            person=parser.parsePerson(fileContent);

        }else if(url.contains("naukri")){
            NaukriParser parser=new NaukriParser();
            person=parser.parsePerson(fileContent);

        }

        if(null!=person) {

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType( MediaType.APPLICATION_JSON );

            //person.setPrimaryEmail("test4@piqube.com");
           //person.setReferrer("New Consult");
            //person.setIsDeleted(0);
            System.out.println("======JSON gson=====\n"+new Gson().toJson(person));

            HttpEntity httpRequest= new HttpEntity( person.toJSON(), headers );
            System.out.println("======JSON=====\n"+person.toJSON());

            RestTemplate template = new RestTemplate();

          //  Person createdProfile=template.postForObject( "http://127.0.0.1:8002/profile/test", httpRequest, Person.class );
          // template.postForObject("http://127.0.0.8002/profiletest",person,Person.class);
            person.set_id(UUID.randomUUID().toString());
            createProfile(person);
            System.out.println("successfully posted::::");

            ExtensionProfile profile=new ExtensionProfile();
            profile.setRequestId(requestId);
            profile.setFilePath(outFilePath);
            profile.setUrl(person.getProfileUrl());
            Users user=userservice.getUserByEmail("test4@piqube.com");
            profile.setProfileId(person.get_id());
            profile.setCreatedBy(user);

            extensionService.saveExtensionProfile(profile);
        }

        return Boolean.TRUE.toString();
    }



    public void createProfile(Person person) {

        extensionService.saveProfile(person);
        System.out.println("**************"+person.toJSON());
    }
    public static String readFile(String localFile) throws Exception {
        FileReader fileReader = new FileReader( localFile );
        BufferedReader reader = new BufferedReader(fileReader);
        StringWriter stringWriter = new StringWriter();
        String line;
        while( ( line = reader.readLine() ) != null )
            stringWriter.write(line);
        return stringWriter.toString();
    }

    @RequestMapping(value="/extension/load",method=RequestMethod.GET)
    public ModelAndView loadProfile(@RequestParam(required = true, value = "requestid") String requestId,
                                    @RequestParam(required = true, value = "url") String url, HttpServletRequest request,
                                    HttpServletResponse response) throws Exception {


        System.out.println("load is called:::::"+requestId);
        System.out.println("forwarding to extension html......");
        ModelAndView modelAndView=new ModelAndView();
        Map<String, Serializable> model = new HashMap<String, Serializable>();
        model.put("ctx", request.getContextPath());
        System.out.println("=======Redirecting========");
        modelAndView.setViewName("index");
        modelAndView.addObject("message","extension");
        return modelAndView;
    }
    @RequestMapping(value="/extension/profile",method=RequestMethod.POST)
    public void postProfile(@RequestParam String requestId) throws Exception {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails)
            username = ((UserDetails) principal).getUsername();
        else
            username = principal.toString();

        LOGGER.debug("Logged in user is:{}",username);
        System.out.println("extension post is called:::::"+requestId.replace("&",""));
        ExtensionProfile extensionProfile=extensionService.getProfileByRequestId(requestId.replace("&",""));

        System.out.println("profile id is::::"+extensionProfile.getProfileId());


    }
}
