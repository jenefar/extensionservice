package com.piqube.marketplace.parser.sites.linkedin;

import com.google.common.collect.Lists;
import com.piqube.marketplace.parser.CrawledSource;
import com.piqube.marketplace.parser.IEnumValue;
import com.piqube.marketplace.parser.Parser;
import com.piqube.marketplace.parser.ParsingException;
import com.piqube.marketplace.parser.parserutils.Arg;
import com.piqube.marketplace.parser.parserutils.HtmlParseUtils;
import com.piqube.marketplace.parser.parserutils._SelectorMetaInfo;
import com.piqube.marketplace.parser.parserutils.parsehelpers.NameValueSplit;
import com.piqube.marketplace.parser.utils.UnaryFunction;
import com.piqube.marketplace.parser.utils.FileUtil;
import com.piqube.model.*;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * @author jenefar
 */
public class LinkedinParser implements Parser {

    public static Logger log = LoggerFactory.getLogger(LinkedinParser.class);

    static FileUtil util =new FileUtil();

    public  enum Sections implements IEnumValue<_SelectorMetaInfo> {

        NAME_SELECTOR( new _SelectorMetaInfo( Arg.E(".full-name") ) ),
        LOCATION_SELECTOR( new _SelectorMetaInfo( Arg.E(".locality") ) ),
        SUMMARY_SELECTOR( new _SelectorMetaInfo( Arg.E(".summary") )),
        WEBSITE_SELECTOR( new _SelectorMetaInfo( Arg.E("#overview-summary-websites li a"),Arg.a("href"),Arg.list() )),
        SKILLS_SELECTOR( new _SelectorMetaInfo( Arg.E("#profile-skills ul li .endorse-item-name a "),Arg.list())),
        MEMBER_ID(new _SelectorMetaInfo(Arg.E(".masthead"),Arg.a("id"),Arg.F(new MemberIdNormalizer()))),
        CURRENT_DESIGNATION_SELECTOR( new _SelectorMetaInfo( Arg.E("#headline .title") ) ),
        CURRENT_COMPANY( new _SelectorMetaInfo( Arg.E("#headline .title") ) ),
        PHOTO_SELECTOR( new _SelectorMetaInfo(Arg.E(".profile-picture img"), Arg.a("src") ) ),
        PROFILE_URL(new _SelectorMetaInfo( Arg.E("link[rel=canonical]"),Arg.a("href"))),
        INTEREST_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-interests .interests-listing li"),Arg.list() )),

        EDUCATION_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-education .education"),Arg.list(),
                Arg.Children(EducationSection.class) ) ),

        EXPERIENCE_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-experience [id^=experience-]"),Arg.list(),
                Arg.Children(ExperienceSection.class) ) ),

        PROJECT_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-projects [id^=project-]"),Arg.list(),
                Arg.Children(ProjectSection.class) ) ),

        AWARD_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-honors .honoraward"),Arg.list(),
                Arg.Children(AwardSection.class) ) ),
        CERTIFICATION_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-certifications [id^=certification-]"),Arg.list(),
                Arg.Children(CertificationSection.class) ) ),
        COURSE_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-courses .section-item"),Arg.list(),
                Arg.Children(CourseSection.class) ) ),

        ORGANIZATION_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-organizations [id^=organization-]"),Arg.list(),
                Arg.Children(OrganizationSection.class) ) ),
        TEST_SCORES_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-test-scores [id^=scores-]"),Arg.list(),
                Arg.Children(TestScoresSection.class) ) ),
        VOLUNTEER_EXPERIENCE_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-volunteering [id^=volunteering-] .experience"),Arg.list(),
                Arg.Children(VolunteerSection.class) ) ),
        VOLUNTEER_OPPORTUNITIES_SELECTOR( new _SelectorMetaInfo( Arg.E("#volunteering-opportunities .opportunities li"),Arg.list() )),

        VOLUNTEER_CAUSES_SELECTOR( new _SelectorMetaInfo( Arg.E("#volunteering-interests .interests li"),Arg.list() )),

        VOLUNTEER_ORGANIZATION_SELECTOR( new _SelectorMetaInfo( Arg.E(".non-profits .volunteering-listing li"),Arg.list() )),

        PATENT_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-patents [id^=patent-]"),Arg.list(),
                Arg.Children(PatentSection.class) ) ),
        LANGUAGE_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-languages [id^=languages-] ol li"),Arg.list(),
                Arg.Children(LanguageSection.class) ) ),
        PUBLICATION_SELECTOR( new _SelectorMetaInfo( Arg.E("#background-publications [id^=publication-]"),Arg.list(),
                Arg.Children(PublicationSection.class) ) ),



        ;
        private _SelectorMetaInfo metaInfo;

        Sections(_SelectorMetaInfo info) {
            this.metaInfo = info;
        }

        public _SelectorMetaInfo get() {
            return metaInfo;
        }

    }

    public enum EducationSection implements IEnumValue<_SelectorMetaInfo> {

        UNIVERSITY(new _SelectorMetaInfo( Arg.E("h4")  ) ),
        DEGREE(new _SelectorMetaInfo( Arg.E("h5 .degree") ) ),
        MAJOR(new _SelectorMetaInfo( Arg.E("h5 .major") ) ),
        GRADE(new _SelectorMetaInfo( Arg.E("h5 .grade") ) ),
        SUMMARY(new _SelectorMetaInfo( Arg.E(".notes") ) ),
        ACTIVITIES(new _SelectorMetaInfo( Arg.E(".activities span") ) ),
        START(new _SelectorMetaInfo( Arg.E(".education-date time"))),
        END(new _SelectorMetaInfo( Arg.E(".education-date time ~time"))),
        ;
        public _SelectorMetaInfo metaInfo;
        private EducationSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }

    public enum ExperienceSection implements IEnumValue<_SelectorMetaInfo> {
        DESIGNATION(new _SelectorMetaInfo( Arg.E("h4") ) ),
        COMPANY_AUTOCOMPLETE(new _SelectorMetaInfo( Arg.E("h4 ~h5 a") ) ),
        COMPANY(new _SelectorMetaInfo( Arg.E("h5") ) ),

        START(new _SelectorMetaInfo( Arg.E(".experience-date-locale time"))),
        END(new _SelectorMetaInfo( Arg.E(".experience-date-locale time ~time"))),

        LOCATION(new _SelectorMetaInfo( Arg.E(".locality"))),
        DESCRIPTION(new _SelectorMetaInfo( Arg.E(".description"))),


        ;
        public _SelectorMetaInfo metaInfo;
        private ExperienceSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }

    public  enum ProjectSection implements IEnumValue<_SelectorMetaInfo> {

        NAME(new _SelectorMetaInfo( Arg.E("h4 span[dir=auto]")) ),
        DURATION(new _SelectorMetaInfo( Arg.E(".projects-date")) ),
        DESCRIPTION(new _SelectorMetaInfo( Arg.E(".description") ) ),
        PROJECT_LINK(new _SelectorMetaInfo( Arg.E("h4 a"),Arg.a("href") ) ),

        ;
        public _SelectorMetaInfo metaInfo;
        private ProjectSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }
    public  enum CertificationSection implements IEnumValue<_SelectorMetaInfo> {

        NAME(new _SelectorMetaInfo( Arg.E("h4 .field-text")) ),
        ISSUER(new _SelectorMetaInfo( Arg.E("h5")) ),
        DURATION(new _SelectorMetaInfo( Arg.E(".certification-date")) ),

        ;
        public _SelectorMetaInfo metaInfo;
        private CertificationSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }

    public  enum CourseSection implements IEnumValue<_SelectorMetaInfo> {

        ORG(new _SelectorMetaInfo( Arg.E("h4")) ),
        DEGREE(new _SelectorMetaInfo( Arg.E(".courses-listing li"),Arg.list()) ),

        ;
        public _SelectorMetaInfo metaInfo;
        private CourseSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }
    public  enum OrganizationSection implements IEnumValue<_SelectorMetaInfo> {

        ORG(new _SelectorMetaInfo( Arg.E("h4")) ),
        POSITION(new _SelectorMetaInfo( Arg.E("h5")) ),
        DURATION(new _SelectorMetaInfo( Arg.E(".organizations-date")) ),
        DESCRIPTION(new _SelectorMetaInfo( Arg.E(".description")) ),

        ;
        public _SelectorMetaInfo metaInfo;
        private OrganizationSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }
    public  enum TestScoresSection implements IEnumValue<_SelectorMetaInfo> {

        TEST_TITLE(new _SelectorMetaInfo(Arg.E("h4"))),
        SCORE(new _SelectorMetaInfo(Arg.E("h5"), Arg.F(new NameValueSplit(":")))),
        DURATION(new _SelectorMetaInfo(Arg.E(".test-scores-date"))),
        DESCRIPTION(new _SelectorMetaInfo(Arg.E(".description"))),;
        public _SelectorMetaInfo metaInfo;

        private TestScoresSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }

        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }
    public  enum LanguageSection implements IEnumValue<_SelectorMetaInfo> {

        LANGUAGE(new _SelectorMetaInfo( Arg.E("h4 span[dir=auto]")) ),
        PROFICIENCY(new _SelectorMetaInfo( Arg.E(".languages-proficiency")) ),

        ;
        public _SelectorMetaInfo metaInfo;
        private LanguageSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }
    public  enum VolunteerSection implements IEnumValue<_SelectorMetaInfo> {

        POSITION(new _SelectorMetaInfo( Arg.E("h4")) ),

        //TODO: for autocompte select from a tag for others select span [dir=auto] property
        ORG_AUTOCOMPLETE(new _SelectorMetaInfo( Arg.E("h5 a[dir=auto]")) ),
        ORG(new _SelectorMetaInfo( Arg.E("h5 span[dir=auto]")) ),
        START(new _SelectorMetaInfo( Arg.E(".volunteering-date-cause time"))),
        END(new _SelectorMetaInfo( Arg.E(".volunteering-date-cause time ~time"))),
        DESCRIPTION(new _SelectorMetaInfo(Arg.E(".description")) ),

        ;
        public _SelectorMetaInfo metaInfo;
        private VolunteerSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }
    public  enum PatentSection implements IEnumValue<_SelectorMetaInfo> {

        TITLE(new _SelectorMetaInfo( Arg.E("h4 span[dir=auto]")) ),
        PATENT_LINK(new _SelectorMetaInfo( Arg.E("h4 a"),Arg.a("href") ) ),

        LOCATION(new _SelectorMetaInfo( Arg.E("h5 span[dir=auto]")) ),
        INVENTORS(new _SelectorMetaInfo( Arg.E(".associated-endorsements ul li"),Arg.list()) ),

        DATE(new _SelectorMetaInfo( Arg.E(".patents-date")) ),
        DESCRIPTION(new _SelectorMetaInfo(Arg.E(".description")) ),

        ;
        public _SelectorMetaInfo metaInfo;
        private PatentSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }
    public  enum PublicationSection implements IEnumValue<_SelectorMetaInfo> {

        TITLE(new _SelectorMetaInfo( Arg.E("h4 span[dir=auto]")) ),
        PUBLICATION_LINK(new _SelectorMetaInfo( Arg.E("h4 a"),Arg.a("href") ) ),
        PUBLISHER(new _SelectorMetaInfo( Arg.E("h5 span[dir=auto]")) ),
        AUTHORS(new _SelectorMetaInfo( Arg.E(".associated-endorsements ul li"),Arg.list()) ),

        DURATION(new _SelectorMetaInfo( Arg.E(".publication-date")) ),
        DESCRIPTION(new _SelectorMetaInfo(Arg.E(".description")) ),

        ;
        public _SelectorMetaInfo metaInfo;
        private PublicationSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }
    public  enum AwardSection implements IEnumValue<_SelectorMetaInfo> {

        NAME(new _SelectorMetaInfo( Arg.E("h4")) ),
        ISSUER(new _SelectorMetaInfo( Arg.E("h5")) ),
        DURATION(new _SelectorMetaInfo( Arg.E(".honors-date")) ),
        DESCRIPTION(new _SelectorMetaInfo( Arg.E(".description") ) ),

        ;
        public _SelectorMetaInfo metaInfo;
        private AwardSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }

    public static class MemberIdNormalizer implements UnaryFunction<String,String> {
        @Override
        public String exec(String input) {
            if (input.contains("-")) {
                String[] str = input.split("-");
                if (str.length > 0)
                    return str[1].trim();
                return null;
            } else {
                return null;
            }
        }
    }
    @Override
    public Person parsePerson(String htmlSource) throws ParsingException {

        Map<Sections, Object> map = HtmlParseUtils.parse(htmlSource, Sections.class);
        Set<Map.Entry<Sections, Object>> entries = map.entrySet();
        for (Map.Entry<Sections, Object> entry : entries) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }

        Set<Map<ProjectSection, Object>> projectFieldMap = (Set<Map<ProjectSection, Object>>) map.get(Sections.PROJECT_SELECTOR);
        Set<Map<EducationSection, Object>> educationFieldMap = (Set<Map<EducationSection, Object>>) map.get(Sections.EDUCATION_SELECTOR);
        Set<Map<ExperienceSection, Object>> experienceFieldMap = (Set<Map<ExperienceSection, Object>>) map.get(Sections.EXPERIENCE_SELECTOR);
        Set<Map<TestScoresSection, Object>> scoresMap = (Set<Map<TestScoresSection, Object>>) map.get(Sections.TEST_SCORES_SELECTOR);
        Set<Map<CertificationSection, Object>> certificationMap = (Set<Map<CertificationSection, Object>>) map.get(Sections.CERTIFICATION_SELECTOR);
        Set<Map<CourseSection, Object>> courseMap = (Set<Map<CourseSection, Object>>) map.get(Sections.COURSE_SELECTOR);
        Set<Map<OrganizationSection, Object>> organizationMap = (Set<Map<OrganizationSection, Object>>) map.get(Sections.ORGANIZATION_SELECTOR);
        Set<Map<LanguageSection, Object>> languageMap = (Set<Map<LanguageSection, Object>>) map.get(Sections.LANGUAGE_SELECTOR);
        Set<Map<PublicationSection, Object>> publicationMap = (Set<Map<PublicationSection, Object>>) map.get(Sections.PUBLICATION_SELECTOR);
        Set<Map<PatentSection, Object>> patentMap = (Set<Map<PatentSection, Object>>) map.get(Sections.PATENT_SELECTOR);
        Set<Map<AwardSection, Object>> awardMap = (Set<Map<AwardSection, Object>>) map.get(Sections.AWARD_SELECTOR);
        Set<Map<VolunteerSection, Object>> volunteerExperienceMap = (Set<Map<VolunteerSection, Object>>) map.get(Sections.VOLUNTEER_EXPERIENCE_SELECTOR);



        Set<String> skillSet = (Set) map.get(Sections.SKILLS_SELECTOR);
        Set<String> volunteerOpportunitiesSet = (Set) map.get(Sections.VOLUNTEER_OPPORTUNITIES_SELECTOR);
        Set<String> volunteerCausesSet = (Set) map.get(Sections.VOLUNTEER_CAUSES_SELECTOR);
        Set<String> volunteerOrganizationsSet = (Set) map.get(Sections.VOLUNTEER_ORGANIZATION_SELECTOR);



        String name = (String) map.get(Sections.NAME_SELECTOR);
        String location = (String) map.get(Sections.LOCATION_SELECTOR);
        String profileSummary = (String) map.get(Sections.SUMMARY_SELECTOR);
        Set websites = (Set) map.get(Sections.WEBSITE_SELECTOR);
        String role = (String) map.get(Sections.CURRENT_DESIGNATION_SELECTOR);
        String currentCompany = (String) map.get(Sections.CURRENT_COMPANY);
        Set interests = (Set) map.get(Sections.INTEREST_SELECTOR);
        String photoUrl = (String) map.get(Sections.PHOTO_SELECTOR);
        String profileUrl = (String) map.get(Sections.PROFILE_URL);
        String memberId = (String) map.get(Sections.MEMBER_ID);


        Person person = new Person();

        if (!util.isEmpty(name)) {
            Name personName = getPersonName(name);
            person.setName(personName);
        }
        if (!util.isEmpty(profileSummary))
            person.setProfileSummary(profileSummary);
        if (!util.isEmpty(role))
            person.setDesignation(role);
        if(!util.isEmpty(profileUrl))
            person.setProfileUrl(profileUrl);
        if(!util.isEmpty(memberId))
            person.setMemberId(memberId);
        if(!util.isEmpty(websites))
            person.setWebsites(Lists.newArrayList(websites));
        if(!util.isEmpty(interests))
            person.setInterests(interests);
        if(!util.isEmpty(photoUrl))
            person.setPhoto_url(photoUrl);
        if(!util.isEmpty(location))
            person.setLocality(location);


        if(!util.isEmpty(skillSet))
            person.setSkills(Lists.newArrayList(skillSet));
        if(!util.isEmpty(volunteerCausesSet))
            person.setVolunteer_causes(Lists.newArrayList(volunteerCausesSet));
        if(!util.isEmpty(volunteerOpportunitiesSet))
            person.setVolunteer_opportunities(Lists.newArrayList(volunteerOpportunitiesSet));
        if(!util.isEmpty(volunteerOrganizationsSet))
            person.setVolunteer_organization(Lists.newArrayList(volunteerOrganizationsSet));
        if (!util.isEmpty(educationFieldMap)) {
            List educationList = getPersonEducation(educationFieldMap);
            person.setEducation(educationList);
        }
        if (!util.isEmpty(experienceFieldMap)) {
            List experienceList = getPersonExperience(experienceFieldMap);
            person.setExperience(experienceList);
        }
        if (!util.isEmpty(projectFieldMap)) {
            List projectList = getPersonProject(projectFieldMap);
            person.setProjects(projectList);
        }
        if (!util.isEmpty(scoresMap)) {
            List scoreList = getPersonTestScores(scoresMap);
           person.setTestScoreList(scoreList);
        }
        if (!util.isEmpty(certificationMap)) {
            List certificationList = getPersonCertification(certificationMap);
            person.setCertifications(certificationList);
        }
        if (!util.isEmpty(courseMap)) {
            List courses = getPersonCourses(courseMap);
            person.setCourses(courses);
        }
        if (!util.isEmpty(publicationMap)) {
            List publications = getPersonPublications(publicationMap);
            person.setPublications(publications);
        }
        if (!util.isEmpty(patentMap)) {
            List patents = getPersonPatents(patentMap);
            person.setPatent(patents);
        }
        if (!util.isEmpty(languageMap)) {
            List languages = getPersonLanguages(languageMap);
            person.setLanguages_known(languages);
        }
        if (!util.isEmpty(organizationMap)) {
            List organizations = getPersonOrganizations(organizationMap);
            person.setOrganizations(organizations);
        }
        if (!util.isEmpty(awardMap)) {
            List awards = getPersonAwards(awardMap);
            person.setHonors(awards);
        }
        if (!util.isEmpty(volunteerExperienceMap)) {
            List volunteerExperienceList = getPersonVolunteerExperience(volunteerExperienceMap);
            person.setVolunteer_experience(volunteerExperienceList);
        }
        
        person.setSource(CrawledSource.LINKEDIN.name());
        person.setCrawledDate(new Date());

        System.out.println("Person:"+person.toJSON());
        return person;
    }

    private List getPersonVolunteerExperience(Set<Map<VolunteerSection, Object>> volunteerExperienceMap) {
        List<VolunteerExperience> volunteerExperiences = new ArrayList();
        if (null != volunteerExperienceMap) {
            for (Map<VolunteerSection, Object> item : volunteerExperienceMap) {
                VolunteerExperience volunteerExperience = new VolunteerExperience();
                String org = (String) item.get(VolunteerSection.ORG);
                String description = (String) item.get(VolunteerSection.DESCRIPTION);
                String position = (String) item.get(VolunteerSection.POSITION);
                String start = (String) item.get(VolunteerSection.START);
                String end = (String) item.get(VolunteerSection.END);

                String org_autocomplete = (String) item.get(VolunteerSection.ORG_AUTOCOMPLETE);

                if(!util.isEmpty(org))
                    volunteerExperience.setOrg(org);
                if(!util.isEmpty(org_autocomplete))
                    volunteerExperience.setOrg(org_autocomplete);
                if(!util.isEmpty(description))
                    volunteerExperience.setDesc(description);
                if(!util.isEmpty(position))
                    volunteerExperience.setPosition(position);
                if(!util.isEmpty(org))
                    volunteerExperience.setOrg(org);
                if (!util.isEmpty(start))
                    volunteerExperience.setStart(start);
                if (!util.isEmpty(end))
                    volunteerExperience.setEnd(end.toString().replace("– ", ""));
                volunteerExperiences.add(volunteerExperience);
            }
        }
        return volunteerExperiences;
    }

    private List getPersonAwards(Set<Map<AwardSection, Object>> awardMap) {
        List<Honors> honors = new ArrayList();
        if (null != awardMap) {
            for (Map<AwardSection, Object> item : awardMap) {
                Honors honor = new Honors();
                String name = (String) item.get(AwardSection.NAME);
                String issuer = (String) item.get(AwardSection.ISSUER);
                String description = (String) item.get(AwardSection.DESCRIPTION);
                String date = (String) item.get(AwardSection.DURATION);

                if(!util.isEmpty(name))
                    honor.setName(name);
                if(!util.isEmpty(issuer))
                    honor.setIssuer(issuer);
                if(!util.isEmpty(description))
                    honor.setDescription(description);
                if(!util.isEmpty(date))
                    honor.setIssueDate(date);

                honors.add(honor);
            }
        }

        return honors;
    }

    private List getPersonOrganizations(Set<Map<OrganizationSection, Object>> organizationMap) {
        List<Organization> organizations = new ArrayList();
        if (null != organizationMap) {
            for (Map<OrganizationSection, Object> item : organizationMap) {
                Organization organization = new Organization();
                String org = (String) item.get(OrganizationSection.ORG);
                String position = (String) item.get(OrganizationSection.POSITION);
                String description = (String) item.get(OrganizationSection.DESCRIPTION);
                String duration = (String) item.get(OrganizationSection.DURATION);


                if(!util.isEmpty(org))
                    organization.setOrganization(org);
                if(!util.isEmpty(position))
                    organization.setPosition(position);
                if(!util.isEmpty(description))
                    organization.setDescription(description);
                if (!util.isEmpty(duration)) {
                    if (duration.toString().contains("–")) {
                        Map map = splitString(duration, "–");
                        organization.setStart((String) map.get("left"));
                        organization.setEnd((String) map.get("right"));
                    } else
                        organization.setStart(duration);

                    if (duration.toString().contains("Starting"))
                        organization.setStart(duration.replace("Starting ", ""));

                }

                organizations.add(organization);
            }
        }
        return organizations;
    }

    private List getPersonLanguages(Set<Map<LanguageSection, Object>> languageMap) {
        List<Language> languages = new ArrayList();
        if (null != languageMap) {
            for (Map<LanguageSection, Object> item : languageMap) {
                Language language = new Language();
                String lang = (String) item.get(LanguageSection.LANGUAGE);
                String proficiency = (String) item.get(LanguageSection.PROFICIENCY);

                if(!util.isEmpty(lang))
                    language.setName(lang);
                if(!util.isEmpty(proficiency))
                    language.setProficiency(proficiency);
                languages.add(language);
            }
        }
        return languages;
    }

    private List getPersonPatents(Set<Map<PatentSection, Object>> patentMap) {
        List<Patent> patents = new ArrayList();
        if (null != patentMap) {
            for (Map<PatentSection, Object> item : patentMap) {
                Patent patent = new Patent();
                String title = (String) item.get(PatentSection.TITLE);
                String location = (String) item.get(PatentSection.LOCATION);
                String description = (String) item.get(PatentSection.DESCRIPTION);
                String link = (String) item.get(PatentSection.PATENT_LINK);
                String date = (String) item.get(PatentSection.DATE);
                Set inventors = (Set) item.get(PatentSection.INVENTORS);

                if (!util.isEmpty(title))
                    patent.setName(title);
                if (!util.isEmpty(description))
                    patent.setDesc(description);
                if (!util.isEmpty(date)) {
                    if (date.toString().contains("Issued"))
                        patent.setDate(date.replace("Issued", ""));
                    else
                        patent.setDate(date.replace("Filed", ""));
                }
                if (!util.isEmpty(location))
                    patent.setAddress(location);
                if (!util.isEmpty(inventors))
                    patent.setAuthors(Lists.newArrayList(inventors));
                if (!util.isEmpty(link)) {
                    try {
                        List<NameValuePair> params = URLEncodedUtils.parse(new URI(link), "UTF-8");
                        for (NameValuePair param : params) {
                            if (param.getName().equals("url")) {
                                patent.setUrl(param.getValue());

                            }
                        }
                    } catch (Exception e) {

                    }
                }

                patents.add(patent);

            }
        }
        return patents;
    }

    private List getPersonPublications(Set<Map<PublicationSection, Object>> publicationMap) {
        List<Publications> publications = new ArrayList();
        if (null != publicationMap) {
            for (Map<PublicationSection, Object> item : publicationMap) {
                Publications publication = new Publications();
                String title = (String) item.get(PublicationSection.TITLE);
                String publisher = (String) item.get(PublicationSection.PUBLISHER);
                String description = (String) item.get(PublicationSection.DESCRIPTION);
                String link = (String) item.get(PublicationSection.PUBLICATION_LINK);
                String date = (String) item.get(PublicationSection.DURATION);

                Set authors = (Set) item.get(PublicationSection.AUTHORS);

                if (!util.isEmpty(title))
                    publication.setName(title);
                if (!util.isEmpty(publisher))
                    publication.setPublisher(publisher);
                if (!util.isEmpty(description))
                    publication.setDesc(description);

                if (!util.isEmpty(date))
                    publication.setDate(date);
                if (!util.isEmpty(title))
                    publication.setName(title);
                if (!util.isEmpty(authors))
                    publication.setAuthors(Lists.newArrayList(authors));
                if (!util.isEmpty(link)) {
                    try {
                        List<NameValuePair> params = URLEncodedUtils.parse(new URI(link), "UTF-8");
                        for (NameValuePair param : params) {
                            if (param.getName().equals("url")) {
                                publication.setUrl(param.getValue());

                            }
                        }
                    } catch (Exception e) {

                    }
                }
                publications.add(publication);
            }
        }
        return publications;
    }
    private List getPersonCourses(Set<Map<CourseSection, Object>> courseMap) {
        List<Courses> courses = new ArrayList();
        if (null != courseMap) {
            for (Map<CourseSection, Object> item : courseMap) {
                String org = (String) item.get(CourseSection.ORG);
                Set degrees = (Set) item.get(CourseSection.DEGREE);

                if (!util.isEmpty(degrees)) {
                    for (Object degree : degrees) {
                        Courses course = new Courses();
                        if (!util.isEmpty(org))
                            course.setOrg(org);

                        if (!util.isEmpty(degree))
                            course.setDegree(degree.toString());
                        courses.add(course);
                    }

                }
            }
        }
        return courses;
    }

    private List getPersonCertification(Set<Map<CertificationSection, Object>> certificationMap) {
        List<Certifications> certificationsList = new ArrayList();
        if (null != certificationMap) {
            for (Map<CertificationSection, Object> item : certificationMap) {
                Certifications certification=new Certifications();
                String name = (String) item.get(CertificationSection.NAME);
                String issuer = (String) item.get(CertificationSection.ISSUER);
                String date = (String) item.get(CertificationSection.DURATION);

                if(!util.isEmpty(name))
                    certification.setName(name);
                if(!util.isEmpty(issuer)) {
                    if(issuer.contains(", License"))
                    {
                        String words[]=issuer.split(", License");
                        certification.setCertifier(words[0]);
                        if(words.length>1)
                            certification.setCertificationNo(words[1]);
                    }
                }
                if (!util.isEmpty(date)) {
                    if (date.contains("Starting")) {
                        certification.setStart(date.replace("Starting ", ""));
                    } else if (date.contains("-")) {
                        Map map = splitString(date, "-");
                        certification.setStart((String) map.get("left"));
                        certification.setEnd((String) map.get("right"));
                    }
                }

                certificationsList.add(certification);
            }
        }
        return certificationsList;
    }

    private List getPersonTestScores(Set<Map<TestScoresSection, Object>> scoresMap) {
        List<TestScore> testScoresList = new ArrayList();
        if (null != scoresMap) {
            for (Map<TestScoresSection, Object> item : scoresMap) {
                TestScore testScore=new TestScore();
                String name = (String) item.get(TestScoresSection.TEST_TITLE);
                String score = (String) item.get(TestScoresSection.SCORE);
                String description= (String) item.get(TestScoresSection.DESCRIPTION);
                String date = (String) item.get(TestScoresSection.DURATION);

                if(!util.isEmpty(name))
                    testScore.setName(name);
                if(!util.isEmpty(score))
                    testScore.setScore(score);
                if(!util.isEmpty(description))
                    testScore.setDescription(description);
                if(!util.isEmpty(date))
                    testScore.setDate(date);

                testScoresList.add(testScore);
            }
        }
        return testScoresList;
    }


    private static List getPersonProject(Set<Map<ProjectSection, Object>> projectFieldMap) {
        List<Projects> projectsList = new ArrayList();
        if (null != projectFieldMap) {
            for (Map<ProjectSection, Object> item : projectFieldMap) {
                Projects project = new Projects();
                String projectName = (String) item.get(ProjectSection.NAME);
                String projectDetail = (String) item.get(ProjectSection.DESCRIPTION);
                String duration = (String) item.get(ProjectSection.DURATION);
                String projectLink = (String) item.get(ProjectSection.PROJECT_LINK);


                if (!util.isEmpty(projectName))
                    project.setName(projectName);
                if (!util.isEmpty(projectDetail))
                    project.setName(projectDetail);
                try {
                    List<NameValuePair> params = URLEncodedUtils.parse(new URI(projectLink), "UTF-8");
                    for (NameValuePair param : params) {
                        if (param.getName().equals("url")) {
                            project.setProject_url(param.getValue());
                        }
                    }

                    if (!util.isEmpty(duration)) {
                        if (duration.contains("Starting")) {
                            project.setStart(duration.replace("Starting ", ""));
                            project.setCurrent(true);
                        } else if (duration.contains("-")) {
                            Map map = splitString(duration, "-");
                            project.setStart((String) map.get("left"));
                            project.setEnd((String) map.get("right"));
                        } else {
                            project.setStart(duration);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                projectsList.add(project);
            }
        }
        return projectsList;
    }

    private static List getPersonExperience(Set<Map<ExperienceSection, Object>> experienceFieldMap) {
        List<Experience> experienceList=new ArrayList();
        if (null != experienceFieldMap) {
            int experienceCount=0;
            for (Map<ExperienceSection, Object> item : experienceFieldMap) {
                Experience experience=new Experience();
                experience.setResumeOrdinal(experienceCount);
                experienceCount=experienceCount+1;
                String company_auto = (String) item.get(ExperienceSection.COMPANY_AUTOCOMPLETE);
                String company = (String) item.get(ExperienceSection.COMPANY);
                String designation = (String) item.get(ExperienceSection.DESIGNATION);
                String summary=(String) item.get(ExperienceSection.DESCRIPTION);
                String location=(String) item.get(ExperienceSection.LOCATION);
                String start = (String) item.get(ExperienceSection.START);
                String end = (String) item.get(ExperienceSection.END);


                if (!util.isEmpty(company_auto))
                    experience.setOrg(company_auto);
                if (!util.isEmpty(company))
                    experience.setOrg(company);
                if (!util.isEmpty(designation))
                    experience.setTitle(designation);
                if(!util.isEmpty(summary))
                    experience.setDesc(summary);
                if(!util.isEmpty(location))
                    experience.setLocation(location);
                if (!util.isEmpty(start))
                    experience.setStart(start);
                if(!util.isEmpty(end)) {
                    experience.setEnd(end);
                }
                else {
                    experience.setCurrent(true);
                    experience.setEnd("Present");
                }
                experienceList.add(experience);
            }
        }
        return experienceList;
    }

    private static List<Education> getPersonEducation(Set<Map<EducationSection, Object>> educationFieldMap) {
        List<Education> educationList = new ArrayList();
        if (null != educationFieldMap) {
            for (Map<EducationSection, Object> item : educationFieldMap) {
                Education education = new Education();
                String university = (String) item.get(EducationSection.UNIVERSITY);
                String degree = (String) item.get(EducationSection.DEGREE);
                String major = (String) item.get(EducationSection.MAJOR);
                String grade = (String) item.get(EducationSection.GRADE);
                String activities = (String) item.get(EducationSection.ACTIVITIES);
                String start = (String) item.get(EducationSection.START);
                String end = (String) item.get(EducationSection.END);
                String summary = (String) item.get(EducationSection.SUMMARY);

                if (!util.isEmpty(university))
                    education.setName(university);
                if (!util.isEmpty(degree))
                    education.setMajor(degree);
                if (!util.isEmpty(major))
                    education.setMajor(major);
                if (!util.isEmpty(summary))
                    education.setMajor(summary);
                if (!util.isEmpty(activities))
                    education.setMajor(activities);
                if (!util.isEmpty(grade))
                    education.setMajor(grade.replace(",", ""));
                if (!util.isEmpty(start))
                    education.setStart(start);
                if (!util.isEmpty(end))
                    education.setEnd(end.toString().replace("– ", ""));

                educationList.add(education);

            }
        }
        return educationList;
    }

    private static Map splitString(String input,String delimiter) {
        Map map = new HashMap();
        if (null != input) {
            String[] words = input.split(delimiter);
            map.put("left", words[0]);
            if (words.length > 1)
                map.put("right", words[1]);
            return map;
        }
        return null;
    }

    private static Name getPersonName(String name) {
        Name personName=new Name();

        if(!util.isEmpty(name)) {
            String[] names = name.split(" ", 2);
            personName.setGiven_name(names[0]);
            if(names.length>1)
                personName.setFamily_name(names[1]);
            return personName;
        }
        return null;
    }

    public static void main(String[] args) throws Exception {

        String pageUrl="https://www.linkedin.com/in/purvi-narang-3498066a?authType=name&authToken=tRAU&trk=prof-sb-browse_map-name";
        pageUrl=pageUrl.replaceAll("-","_");
        URL url = new URL(pageUrl);
        String host  = url.getHost();
        String path = url.getPath();
        if(host.matches(".*.linkedin.com") || host.matches(".*linkedin.com")) {
            System.out.println("Path is::::"+path);
            if(path.matches("/pub/dir/.*")) {
                System.out.println("false:::::::");
            } else if(path.matches("/\\w{2}/\\w{1,30}") || path.matches("/pub/.*") || path.matches("/profile/view.*")) {
                System.out.println("true:::::::");
            }
        }
        //http://www.techgig.com/manojbehera3
        //http://www.techgig.com/overview.php?uid=78957 - deactivated user
       /* FileUtil util=new FileUtil();
        //String htmlSource=util.requestUrl(new URI("https://www.linkedin.com/pub/anonymous-test/63/854/9a1"));
String fileContent=readFile("/tmp/upload/b0988218-c629-434e-8bee-fc4c8973cafe");
        LinkedinParser parser=new LinkedinParser();
        if(null!=fileContent)
            parser.parsePerson(fileContent);
        else
            System.out.println("user is deactivated");*/

    }
    public static String readFile(String localFile) throws Exception {
        FileReader fileReader = new FileReader( localFile );
        BufferedReader reader = new BufferedReader(fileReader);
        StringWriter stringWriter = new StringWriter();
        String line;
        while( ( line = reader.readLine() ) != null )
            stringWriter.write(line);
        return stringWriter.toString();
    }


}
