package com.piqube.marketplace.parser.sites.hackerearth;


import com.piqube.marketplace.parser.IEnumValue;
import com.piqube.marketplace.parser.Parser;
import com.piqube.marketplace.parser.ParsingException;
import com.piqube.marketplace.parser.parserutils.Arg;
import com.piqube.marketplace.parser.parserutils.HtmlParseUtils;
import com.piqube.marketplace.parser.parserutils._SelectorMetaInfo;
import com.piqube.marketplace.parser.utils.UnaryFunction;
import com.piqube.marketplace.parser.utils.FileUtil;
import com.piqube.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author jenefar
 */
public class HackerEarthParser implements Parser
{

    public static Logger log = LoggerFactory.getLogger(HackerEarthParser.class);

    static FileUtil fileUtil=new FileUtil();

    public  enum Sections implements IEnumValue<_SelectorMetaInfo> {


        NAME_SELECTOR( new _SelectorMetaInfo( Arg.E(".profile-card h1") ) ),
        LOCATION_SELECTOR( new _SelectorMetaInfo( Arg.E(".track-current-location") ) ),

        CURRENT_DESIGNATION_SELECTOR( new _SelectorMetaInfo( Arg.E("span[property=v:title]") ) ),
        CURRENT_COMPANY( new _SelectorMetaInfo( Arg.E("span[property=v:affiliation]") ) ),

        TECHNICAL_FRAMEWORKS_SELECTOR( new _SelectorMetaInfo( Arg.E(".skill-snippet:contains(Frameworks) span"))),
        TECHNICAL_TOOLS_SELECTOR( new _SelectorMetaInfo( Arg.E(".skill-snippet:contains(Tools) span"))),


        PROJECT_SELECTOR( new _SelectorMetaInfo( Arg.E("[id^=project-]"),Arg.list(),
                Arg.Children(ProjectSection.class) ) ),
        EXPERIENCE_SELECTOR( new _SelectorMetaInfo( Arg.E("[id^=exp-]"),Arg.list(),
                Arg.Children(ExperienceSection.class) ) ),
        EDUCATION_SELECTOR( new _SelectorMetaInfo( Arg.E("[id^=edu-]"),Arg.list(),
                Arg.Children(EducationSection.class) ) ),
        ACHIEVEMENTS_SELECTOR( new _SelectorMetaInfo( Arg.E("[id^=achieve-]"),Arg.list(),
                Arg.Children(AchievementSection.class) ) ),
        SOCIAL_LINKS_SELECTOR( new _SelectorMetaInfo( Arg.E(".share-section .medium-margin-bottom a"),Arg.a("href"),Arg.list() )),

        PHOTO_URL(new _SelectorMetaInfo( Arg.E("meta[property=og:image]"),Arg.a("content"))),
        PROFILE_URL(new _SelectorMetaInfo( Arg.E("meta[property=og:url]"),Arg.a("content"))),


        ;
        private _SelectorMetaInfo metaInfo;

        Sections(_SelectorMetaInfo info) {
            this.metaInfo = info;
        }

        public _SelectorMetaInfo get() {
            return metaInfo;
        }

    }

    public enum EducationSection implements IEnumValue<_SelectorMetaInfo> {

        DURATION(new _SelectorMetaInfo( Arg.E(".content [class$=small]"))),//,Arg.F(new DurationNormalizer()))),
        DEGREE(new _SelectorMetaInfo( Arg.E(".content [class$=dark]"))),//Arg.F(new DegreeNormalizer()) ) ),
        UNIVERSITY(new _SelectorMetaInfo( Arg.E(".content .heading"))) ,//Arg.F(new UniversityNormalizer()) ) ),
        DEGREE_LEVEL(new _SelectorMetaInfo( Arg.E(".content [class$=dark]") ) ),
        DESCRIPTION(new _SelectorMetaInfo( Arg.E(".full-content .desc") ) ),
        CGPA(new _SelectorMetaInfo( Arg.E(".skill-list:contains(CGPA:)") ) ),
        SKILLS(new _SelectorMetaInfo( Arg.E(".skill-list:contains(Skills:)") ,Arg.ownText())),


        ;
        public _SelectorMetaInfo metaInfo;
        private EducationSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }

    public enum AchievementSection implements IEnumValue<_SelectorMetaInfo> {

        DURATION(new _SelectorMetaInfo( Arg.E(".content [class$=small]"))),//,Arg.F(new DurationNormalizer()))),
        ISSUER(new _SelectorMetaInfo( Arg.E(".content [class$=dark]"))),//Arg.F(new DegreeNormalizer()) ) ),
        TITLE(new _SelectorMetaInfo( Arg.E(".content .heading"))) ,//Arg.F(new UniversityNormalizer()) ) ),
        DESCRIPTION(new _SelectorMetaInfo( Arg.E(".full-content .desc") ) ),

        ;
        public _SelectorMetaInfo metaInfo;
        private AchievementSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }

    public enum ExperienceSection implements IEnumValue<_SelectorMetaInfo> {
        DESIGNATION(new _SelectorMetaInfo( Arg.E("[class$=float-left] [class$=float-left]") ) ),
        DURATION(new _SelectorMetaInfo( Arg.E("[class$=float-left] [class$=light small]"))),
        COMPANY(new _SelectorMetaInfo( Arg.E("[class$=float-left] [class$=regular dark]"))),
        LOCATION(new _SelectorMetaInfo( Arg.E("[class$=float-left] [class$=regular dark]"))),
        DESCRIPTION(new _SelectorMetaInfo( Arg.E(".full-content .desc"))),


        // INDUSTRY(new _SelectorMetaInfo( Arg.E(".pro-txt:contains(Industry:)"),Arg.F(new ReplaceString("at ","")) ,Arg.ownText())),

        ;
        public _SelectorMetaInfo metaInfo;
        private ExperienceSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }

    public  enum ProjectSection implements IEnumValue<_SelectorMetaInfo> {

        NAME(new _SelectorMetaInfo( Arg.E(".content .heading") ) ),
        DURATION(new _SelectorMetaInfo( Arg.E("[class$=float-left] [class$=light small]"))),
        //Arg.F((new DurationSplitter("to")).left("start").right("end")) ) ),
        LINK(new _SelectorMetaInfo( Arg.E(".skill-list a") ) ),
        SKILLS(new _SelectorMetaInfo( Arg.E(".skill-list:contains(Skills:)") ,Arg.ownText())),
        DETAIL(new _SelectorMetaInfo( Arg.E(".full-content .desc") ) ),

        ;
        public _SelectorMetaInfo metaInfo;
        private ProjectSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }

    public static class MemberIdFetcher implements UnaryFunction<String, String> {
        @Override
        public String exec(String input) {
            try {
                Pattern pattern = Pattern.compile("selected_uid=(.*?)','Y'");
                Matcher matcher = pattern.matcher(input);
                while (matcher.find()) {
                    return matcher.group(1);
                }
            } catch (Exception e) {
                return null;
            }
            return null;
        }
    }
    public static class CompanyNameNormalizer implements UnaryFunction<String, String> {
        @Override
        public String exec(String input) {
            try {
                String[] words = input.split(" at ", 2);
                String[] companyName = words[1].split("\\(", 2);
                return companyName[0].replace(" at ", "").trim();
            } catch (Exception e) {
                return input;
            }
        }
    }

    //ME/M.Tech at Satyabama university Field of Study: Computer Science, from May 2010 till May 2013
    public static class DegreeNormalizer implements UnaryFunction<String,String> {
        @Override
        public String exec(String input) {
            try {
                String[] words = input.split("Field of Study:");
                String[] degree = words[1].split(", from");
                if(degree[0].contains("Information not supplied"))
                    return null;
                else
                    return degree[0].trim();
            } catch (Exception e) {
                return null;
            }
        }
    }


    //regex is used as the university is specified under anchor tags for some cases and for some cases as a plain text.
    public static class UniversityNormalizer implements UnaryFunction<String,String> {
        @Override
        public String exec(String input) {
            try {
                Pattern pattern = Pattern.compile("at (.*?)Field of Study: ");
                Matcher matcher = pattern.matcher(input);
                while (matcher.find()) {
                    return matcher.group(1);
                }
            } catch (Exception e) {
                return null;
            }
            return null;
        }
    }

    public static class DurationNormalizer implements UnaryFunction<String,String>{
        @Override
        public String exec(String input) {
            try {
                String[] words = input.split("Field of Study:");
                String[] duration = words[1].split(", from");
                return duration[1].trim();
            } catch (Exception e) {
                return null;
            }
        }
    }

    public static Map durationSplitter(String input,String delimiter,String condition) {

        Map duration = null;
        try {
            String[] words = input.split(delimiter);
            String[] fromDate = new String[0];
            duration = new HashMap();
            if (condition.contains("project")) {
                fromDate = words[0].split("(?i)\\( From");
                if (null != fromDate[1])
                    duration.put("start", fromDate[1]);
                if (null != words[1])
                    duration.put("end", words[1].replace(")", ""));
            }
            if (condition.contains("education")) {
                if (null != words[0])
                    duration.put("start", words[0]);
                if (null != words[1])
                    duration.put("end", words[1]);

            }
            if (condition.contains("job")) {
                fromDate = words[0].split("(?i)\\( from ");

                if (null != fromDate[1])
                    duration.put("start", fromDate[1]);
                if (null != words[1])
                    duration.put("end", words[1].replace(")", ""));
            }

            return duration;
        } catch (Exception e) {
            e.printStackTrace();
            return duration;
        }
    }

    @Override
    public Person parsePerson(String htmlSource) throws ParsingException {

        Map<Sections, Object> map = HtmlParseUtils.parse(htmlSource, Sections.class);
        Set<Map.Entry<Sections, Object>> entries = map.entrySet();
        for (Map.Entry<Sections, Object> entry : entries) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }


       /* Set<Map<ProjectSection, Object>> projectFieldMap = (Set<Map<ProjectSection, Object>>) map.get(Sections.PROJECT_SELECTOR);
        Set<Map<EducationSection, Object>> educationFieldMap = (Set<Map<EducationSection, Object>>) map.get(Sections.EDUCATION_SELECTOR);
        Set<Map<ExperienceSection, Object>> experienceFieldMap = (Set<Map<ExperienceSection, Object>>) map.get(Sections.EXPERIENCE_SELECTOR);
        Set<String> skillSet = (Set) map.get(Sections.SKILLS_SELECTOR);


        String name = (String) map.get(Sections.NAME_SELECTOR);
        String city = (String) map.get(Sections.CITY_SELECTOR);
        String country = (String) map.get(Sections.COUNTRY_SELECTOR);
        String profileSummary = (String) map.get(Sections.SUMMARY_SELECTOR);
        String website = (String) map.get(Sections.WEBSITE_SELECTOR);
        String role = (String) map.get(Sections.CURRENT_DESIGNATION_SELECTOR);
        String currentCompany = (String) map.get(Sections.CURRENT_COMPANY);
        String totalWorkExperience = (String) map.get(Sections.TOTAL_WORK_EXPERIENCE);
        String interest = (String) map.get(Sections.INTEREST_SELECTOR);
        Set specialization = (Set) map.get(Sections.SPECIALIZATION_SELECTOR);
        String photoUrl = (String) map.get(Sections.PHOTO_URL);
        String profileUrl = (String) map.get(Sections.PROFILE_URL);
        String memberId = (String) map.get(Sections.MEMBER_ID);




        Person person = new Person();

        person.setCity(getPersonCity((String) map.get(Sections.CITY_SELECTOR)));
        if (!fileUtil.isEmpty(name)) {
            Name personName = getPersonName(name);
            person.setName(personName);
        }
        if (!fileUtil.isEmpty(city))
            person.setCity(getPersonCity(city));
        if (!fileUtil.isEmpty(country))
            person.setCountry((String) map.get(Sections.COUNTRY_SELECTOR));
        if (!fileUtil.isEmpty(profileSummary))
            person.setProfileSummary(profileSummary);
        if (!fileUtil.isEmpty(role))
            person.setDesignation(role);
        if (!fileUtil.isEmpty(photoUrl))
            person.setPhoto_url(photoUrl);
        if (!fileUtil.isEmpty(educationFieldMap)) {
            List educationList = getPersonEducation(educationFieldMap);
            person.setEducation(educationList);
        }
        if (!fileUtil.isEmpty(experienceFieldMap)) {
            List experienceList = getPersonExperience(experienceFieldMap);
            person.setExperience(experienceList);
        }
        if (!fileUtil.isEmpty(projectFieldMap)) {
            List projectList = getPersonProject(projectFieldMap);
            person.setProjects(projectList);
        }
        if (!fileUtil.isEmpty(skillSet))
            person.setSkills(Lists.newArrayList(skillSet));

        if (!fileUtil.isEmpty(website))
            person.setWebsites(new ArrayList(Arrays.asList(website)));
        if (!fileUtil.isEmpty(interest))
            person.setInterests(interest);

        if(!fileUtil.isEmpty(specialization))
            person.setAreaOfSpecialization(Lists.newArrayList(specialization));

        if(!fileUtil.isEmpty(profileUrl))
            person.setProfileUrl(profileUrl);

        if(!fileUtil.isEmpty(memberId))
            person.setMemberId(memberId);

        person.setSource(CrawledSource.TECHGIG.name());
        // person.setCrawledDate(new Date());

        System.out.println("Person:"+person.toJSON());
       */ return null;
    }


    private static List getPersonProject(Set<Map<ProjectSection, Object>> projectFieldMap) {
        List<Projects> projectsList=new ArrayList();
        if (null != projectFieldMap) {
            for (Map<ProjectSection, Object> item : projectFieldMap) {
                Projects project=new Projects();
                String projectName = (String) item.get(ProjectSection.NAME);
                String projectDetail = (String) item.get(ProjectSection.DETAIL);
                String duration = (String) item.get(ProjectSection.DURATION);

               /* String projectRole = (String) item.get(ProjectSection.ROLE);
                String roleDescription = (String) item.get(ProjectSection.KEY_RESPONSIBILITY);
                String duration = (String) item.get(ProjectSection.DURATION);
                String org=(String) item.get(ProjectSection.ORG);
                if (null != projectName && !projectName.isEmpty())
                    project.setName(projectName);
                if (null != org && !org.isEmpty())
                    project.setOrg(org);
                if (null != projectDetail && !projectDetail.isEmpty())
                    project.setDesc(projectDetail);
                if (null != projectRole && !projectRole.isEmpty())
                    project.setRole(projectRole);
                if (null != roleDescription && !roleDescription.isEmpty() && !"Information not supplied".equals(roleDescription))
                    project.setRoleDescription(roleDescription);*/
                if (null != duration && !duration.isEmpty()) {
                    try{
                        Map durationVal = durationSplitter(duration, " to ","project");

                        if (durationVal.size()>0) {
                            if (null != durationVal.get("start")) {
                                String[] startMonth = String.valueOf(durationVal.get("start")).split(",");
                                String startDate = (String) DateParser.monthMapper().get(startMonth[0].trim());
                                project.setStart(startDate +" "+ startMonth[1].replace(",", ""));
                            }
                            if (null != durationVal.get("end"))
                                if (durationVal.get("end").toString().contains("Present")) {
                                    //techgigDuration.setCurrent(true);
                                }
                                else {
                                    String[] endMonth = String.valueOf(durationVal.get("end")).split(",");
                                    String endDate = (String) DateParser.monthMapper().get(endMonth[0].trim());
                                    project.setEnd(endDate +" "+ endMonth[1].replace(",", ""));

                                }
                        }

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }

                projectsList.add(project);

            }}
        return projectsList;
    }

    private static List getPersonExperience(Set<Map<ExperienceSection, Object>> experienceFieldMap) {
        List<Experience> experienceList=new ArrayList();
        if (null != experienceFieldMap) {
            int experienceCount=0;
            for (Map<ExperienceSection, Object> item : experienceFieldMap) {
                Experience experience=new Experience();
                experience.setResumeOrdinal(experienceCount);
                experienceCount=experienceCount+1;
                String company = (String) item.get(ExperienceSection.COMPANY);
                String designation = (String) item.get(ExperienceSection.DESIGNATION);
                //String industry = (String) item.get(ExperienceSection.INDUSTRY);
                String duration = (String) item.get(ExperienceSection.DURATION);

                if (null != company && !company.isEmpty())
                    experience.setOrg(company);
                if (null != designation && !designation.isEmpty())
                    experience.setTitle(designation);
                if (null != duration && !duration.isEmpty()) {
                    try{
                        Map durationVal = durationSplitter(duration, " till ","job");
                        if (durationVal.size()>0) {
                            if (null != durationVal.get("start")) {
                                String[] startMonth = String.valueOf(durationVal.get("start")).split(" ");
                                String startDate = (String) DateParser.monthMapper().get(startMonth[0].trim());
                                experience.setStart(startDate +" "+ startMonth[1].replace(",", ""));
                            }
                            if (null != durationVal.get("end"))
                                if (durationVal.get("end").toString().contains("now")) {
                                    // experience.isJobCurrent();
                                }
                                else {
                                    String[] endMonth = String.valueOf(durationVal.get("end")).split(" ");
                                    String endDate = (String) DateParser.monthMapper().get(endMonth[0].trim());
                                    experience.setEnd(endDate +" "+ endMonth[1].replace(",", ""));
                                }
                        }


                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }
                experienceList.add(experience);
            }
        }
        return experienceList;
    }

    private static List<Education> getPersonEducation(Set<Map<EducationSection, Object>> educationFieldMap) {
        List<Education> educationList=new ArrayList();
        if (null != educationFieldMap) {
            for (Map<EducationSection, Object> item : educationFieldMap) {
                Education education = new Education();
                String university = (String) item.get(EducationSection.UNIVERSITY);
                String degree = (String) item.get(EducationSection.DEGREE);
                String degreeLevel = (String) item.get(EducationSection.DEGREE_LEVEL);
                String duration = (String) item.get(EducationSection.DURATION);

                if (null != university && !university.isEmpty())
                    education.setName(university);
                if (null != degree && !degree.isEmpty())
                    education.setMajor(degree);
                if (null != degreeLevel && !degreeLevel.isEmpty())
                    education.setDegree(degreeLevel);
                if (null != duration && !duration.isEmpty()) {
                    try {
                        Map durationVal = durationSplitter(duration, " till ", "education");

                        if (durationVal.size() > 0) {
                            if (null != durationVal.get("start")) {
                                String[] startMonth = String.valueOf(durationVal.get("start")).split(" ");
                                String startDate = (String) DateParser.monthMapper().get(startMonth[0].trim());
                                education.setStart(startDate +" "+ startMonth[1].replace(",", ""));
                            }
                            if (null != durationVal.get("end"))
                                if (durationVal.get("end").toString().contains("now")) {
                                    education.setCurrent(true);
                                } else {
                                    String[] endMonth = String.valueOf(durationVal.get("end")).split(" ");
                                    String endDate = (String) DateParser.monthMapper().get(endMonth[0].trim());
                                    education.setEnd(endDate +" "+ endMonth[1].replace(",", ""));
                                }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                educationList.add(education);

            }
        }
        return educationList;
    }

    private static String getPersonCity(String city) {
        //for "Bengaluru / Bangalore~|~" split by "/" and get first word
        if(null!=city){
            if (city.contains("/")) {
                String[] cityStr = city.split("/");
                return cityStr[0];
            }
            //for "Kolkata~|~" replace the special characters
            else
                return city.replaceAll("[^a-zA-Z]+", "");}
        return null;
    }

    private static Name getPersonName(String name) {
        Name personName=new Name();

        if(!fileUtil.isEmpty(name)) {
            String[] names = name.split(" ", 2);
            personName.setGiven_name(names[0]);
            if(names.length>1)
                personName.setFamily_name(names[1]);
            return personName;
        }
        return null;
    }

    public static void main(String[] args) throws Exception {

        FileUtil util=new FileUtil();
        URL url = new URL("http://in.linkedin.com/pub/jenefar-augustinal/15/bb3/590");

        System.out.println("URL:- " +url);
        URLConnection connection = url.openConnection();

        System.out.println(connection.getLastModified());
        System.out.println(connection.getIfModifiedSince());
        String htmlSource=util.requestUrl(new URI("https://www.hackerearth.com/users/iol/"));
        HackerEarthParser parser=new HackerEarthParser();
        if(null!=htmlSource)
            parser.parsePerson(htmlSource);
        else
            System.out.println("user is deactivated");

    }
    public static String readFile(String localFile) throws Exception {
        FileReader fileReader = new FileReader( localFile );
        BufferedReader reader = new BufferedReader(fileReader);
        StringWriter stringWriter = new StringWriter();
        String line;
        while( ( line = reader.readLine() ) != null )
            stringWriter.write(line);
        return stringWriter.toString();
    }



}
