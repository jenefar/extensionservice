package com.piqube.marketplace.parser.sites.techgig;


import com.google.common.collect.Lists;
import com.piqube.marketplace.parser.CrawledSource;
import com.piqube.marketplace.parser.IEnumValue;
import com.piqube.marketplace.parser.Parser;
import com.piqube.marketplace.parser.ParsingException;
import com.piqube.marketplace.parser.parserutils.Arg;
import com.piqube.marketplace.parser.parserutils.HtmlParseUtils;
import com.piqube.marketplace.parser.parserutils._SelectorMetaInfo;
import com.piqube.marketplace.parser.parserutils.parsehelpers.NameValueSplit;
import com.piqube.marketplace.parser.parserutils.parsehelpers.ReplaceString;
import com.piqube.marketplace.parser.utils.UnaryFunction;
import com.piqube.marketplace.parser.utils.FileUtil;
import com.piqube.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.StringWriter;
import java.net.URI;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author jenefar
 */
public class TechGigParser implements Parser
{

    public static Logger log = LoggerFactory.getLogger(TechGigParser.class);

    static FileUtil fileUtil=new FileUtil();

    public  enum Sections implements IEnumValue<_SelectorMetaInfo> {


        NAME_SELECTOR( new _SelectorMetaInfo( Arg.E("span[property=v:name]") ) ),
        CITY_SELECTOR( new _SelectorMetaInfo( Arg.E("span[property=v:city]") ) ),
        COUNTRY_SELECTOR( new _SelectorMetaInfo( Arg.E("span[property=v:country-name]") ) ),

        CURRENT_DESIGNATION_SELECTOR( new _SelectorMetaInfo( Arg.E("span[property=v:title]") ) ),
        CURRENT_COMPANY( new _SelectorMetaInfo( Arg.E("span[property=v:affiliation]") ) ),

        SPECIALIZATION_SELECTOR( new _SelectorMetaInfo( Arg.E(".pro-lft-txt:contains(Area of Specialization) ~ .pro-rt-txt"),Arg.list())),
        SKILLS_SELECTOR( new _SelectorMetaInfo( Arg.E(".pro-lft-txt:contains(My Specialities) ~ .pro-rt-txt a"),Arg.list())),
        TOPICS_SELECTOR( new _SelectorMetaInfo( Arg.E(".pro-lft-txt:contains(My Topics) ~ .pro-rt-txt a"),Arg.list())),

        SUMMARY_SELECTOR( new _SelectorMetaInfo( Arg.E(".summari"),Arg.F(new NameValueSplit() ) )),

        PROJECT_SELECTOR( new _SelectorMetaInfo( Arg.E(".pro-info"),Arg.list(),
        Arg.Children(ProjectSection.class) ) ),
        EXPERIENCE_SELECTOR( new _SelectorMetaInfo( Arg.E(".projects:has(h2:contains(Experience)) .pro-info"),Arg.list(),
                Arg.Children(ExperienceSection.class) ) ),
        EDUCATION_SELECTOR( new _SelectorMetaInfo( Arg.E(".projects:has(h2:contains(Education)) .pro-info"),Arg.list(),
        Arg.Children(EducationSection.class) ) ),
        WEBSITE_SELECTOR( new _SelectorMetaInfo( Arg.E(".pro-lft-txt:contains(Website) ~ .pro-rt-txt a") )),
        INTEREST_SELECTOR( new _SelectorMetaInfo( Arg.E(".pro-lft-txt:contains(Interest) ~ .pro-rt-txt") )),
        TOTAL_WORK_EXPERIENCE( new _SelectorMetaInfo( Arg.E(".pro-lft-txt:contains(Total Work) ~ .pro-rt-txt") )),
        PHOTO_URL(new _SelectorMetaInfo( Arg.E(".user-pic img"),Arg.a("src"))),
        PROFILE_URL(new _SelectorMetaInfo( Arg.E("a[rel~=url]"))),
        MEMBER_ID(new _SelectorMetaInfo( Arg.E(".user-dtl a"),Arg.a("onclick"),Arg.F(new MemberIdFetcher()))),

        ;
        private _SelectorMetaInfo metaInfo;

         Sections(_SelectorMetaInfo info) {
            this.metaInfo = info;
        }

        public _SelectorMetaInfo get() {
            return metaInfo;
        }

    }

    public enum EducationSection implements IEnumValue<_SelectorMetaInfo> {

        DURATION(new _SelectorMetaInfo( Arg.E(".pro-txt"),Arg.F(new DurationNormalizer()))),
        DEGREE(new _SelectorMetaInfo( Arg.E(".pro-txt"),Arg.F(new DegreeNormalizer()) ) ),
        UNIVERSITY(new _SelectorMetaInfo( Arg.E(".pro-txt") ,Arg.F(new UniversityNormalizer()) ) ),
        DEGREE_LEVEL(new _SelectorMetaInfo( Arg.E(".pro-txt b") ) ),

        ;
        public _SelectorMetaInfo metaInfo;
        private EducationSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }

    public enum ExperienceSection implements IEnumValue<_SelectorMetaInfo> {
        DESIGNATION(new _SelectorMetaInfo( Arg.E(".pro-txt span[class=desi-cur]") ) ),
        DURATION(new _SelectorMetaInfo( Arg.E(".pro-txt span[class=dt-clr]"))),
        //TODO: <div class="pro-txt"><span class="desi-cur">Database Administrator (DBA)</span> at <a href="http://www.techgig.com/company.php?company_id=1395"> Infosys Limited</a>
        COMPANY(new _SelectorMetaInfo( Arg.E(".pro-txt"),Arg.F(new CompanyNameNormalizer()) ) ),
        INDUSTRY(new _SelectorMetaInfo( Arg.E(".pro-txt:contains(Industry:)"),Arg.F(new ReplaceString("at ","")) ,Arg.ownText())),

        ;
        public _SelectorMetaInfo metaInfo;
        private ExperienceSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }

    public  enum ProjectSection implements IEnumValue<_SelectorMetaInfo> {

        NAME(new _SelectorMetaInfo( Arg.E(".cur-pro-hd") ,Arg.ownText()) ),
        DURATION(new _SelectorMetaInfo( Arg.E(".cur-pro-hd span[class=dt-clr]")) ),
                //Arg.F((new DurationSplitter("to")).left("start").right("end")) ) ),
        ROLE(new _SelectorMetaInfo( Arg.E(".rol-com .desi-cur") ) ),
        ORG(new _SelectorMetaInfo( Arg.E(".rol-com") ,Arg.F(new NameValueSplit(" at ")))),
        DETAIL(new _SelectorMetaInfo( Arg.E(".pro-dtl") ) ),
        KEY_RESPONSIBILITY(new _SelectorMetaInfo( Arg.E(".pro-key-txt") ,Arg.F(new NameValueSplit())) ),

        ;
        public _SelectorMetaInfo metaInfo;
        private ProjectSection(_SelectorMetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }
        public _SelectorMetaInfo get() {
            return metaInfo;
        }
    }

    public static class MemberIdFetcher implements UnaryFunction<String, String> {
        @Override
        public String exec(String input) {
            try {
                Pattern pattern = Pattern.compile("selected_uid=(.*?)','Y'");
                Matcher matcher = pattern.matcher(input);
                while (matcher.find()) {
                    return matcher.group(1);
                }
            } catch (Exception e) {
                return null;
            }
            return null;
        }
    }
    public static class CompanyNameNormalizer implements UnaryFunction<String, String> {
        @Override
        public String exec(String input) {
            try {
                String[] words = input.split(" at ", 2);
                String[] companyName = words[1].split("\\(", 2);
                return companyName[0].replace(" at ", "").trim();
            } catch (Exception e) {
                return input;
            }
        }
    }

    //ME/M.Tech at Satyabama university Field of Study: Computer Science, from May 2010 till May 2013
    public static class DegreeNormalizer implements UnaryFunction<String,String> {
        @Override
        public String exec(String input) {
            try {
                String[] words = input.split("Field of Study:");
                String[] degree = words[1].split(", from");
                if(degree[0].contains("Information not supplied"))
                    return null;
                else
                    return degree[0].trim();
            } catch (Exception e) {
                return null;
            }
        }
    }


    //regex is used as the university is specified under anchor tags for some cases and for some cases as a plain text.
    public static class UniversityNormalizer implements UnaryFunction<String,String> {
        @Override
        public String exec(String input) {
            try {
                Pattern pattern = Pattern.compile("at (.*?)Field of Study: ");
                Matcher matcher = pattern.matcher(input);
                while (matcher.find()) {
                    return matcher.group(1);
                }
            } catch (Exception e) {
                return null;
            }
            return null;
        }
    }

    public static class DurationNormalizer implements UnaryFunction<String,String>{
        @Override
        public String exec(String input) {
            try {
                String[] words = input.split("Field of Study:");
                String[] duration = words[1].split(", from");
                return duration[1].trim();
            } catch (Exception e) {
                return null;
            }
        }
    }

    public static Map durationSplitter(String input,String delimiter,String condition) {

        Map duration = null;
        try {
            String[] words = input.split(delimiter);
            String[] fromDate = new String[0];
            duration = new HashMap();
            if (condition.contains("project")) {
                fromDate = words[0].split("(?i)\\( From");
                if (null != fromDate[1])
                    duration.put("start", fromDate[1]);
                if (null != words[1])
                    duration.put("end", words[1].replace(")", ""));
            }
            if (condition.contains("education")) {
                if (null != words[0])
                    duration.put("start", words[0]);
                if (null != words[1])
                    duration.put("end", words[1]);

            }
            if (condition.contains("job")) {
                fromDate = words[0].split("(?i)\\( from ");

                if (null != fromDate[1])
                    duration.put("start", fromDate[1]);
                if (null != words[1])
                    duration.put("end", words[1].replace(")", ""));
            }

            return duration;
        } catch (Exception e) {
            e.printStackTrace();
            return duration;
        }
    }

    @Override
    public Person parsePerson(String htmlSource) throws ParsingException {

        Map<Sections, Object> map = HtmlParseUtils.parse(htmlSource, Sections.class);
        /*Set<Map.Entry<Sections, Object>> entries = map.entrySet();
        for (Map.Entry<Sections, Object> entry : entries) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }*/

        Set<Map<ProjectSection, Object>> projectFieldMap = (Set<Map<ProjectSection, Object>>) map.get(Sections.PROJECT_SELECTOR);
        Set<Map<EducationSection, Object>> educationFieldMap = (Set<Map<EducationSection, Object>>) map.get(Sections.EDUCATION_SELECTOR);
        Set<Map<ExperienceSection, Object>> experienceFieldMap = (Set<Map<ExperienceSection, Object>>) map.get(Sections.EXPERIENCE_SELECTOR);
        Set<String> skillSet = (Set) map.get(Sections.SKILLS_SELECTOR);


        String name = (String) map.get(Sections.NAME_SELECTOR);
        String city = (String) map.get(Sections.CITY_SELECTOR);
        String country = (String) map.get(Sections.COUNTRY_SELECTOR);
        String profileSummary = (String) map.get(Sections.SUMMARY_SELECTOR);
        String website = (String) map.get(Sections.WEBSITE_SELECTOR);
        String role = (String) map.get(Sections.CURRENT_DESIGNATION_SELECTOR);
        String currentCompany = (String) map.get(Sections.CURRENT_COMPANY);
        String totalWorkExperience = (String) map.get(Sections.TOTAL_WORK_EXPERIENCE);
        String interest = (String) map.get(Sections.INTEREST_SELECTOR);
        Set specialization = (Set) map.get(Sections.SPECIALIZATION_SELECTOR);
        Set topics=(Set) map.get(Sections.TOPICS_SELECTOR);
        String photoUrl = (String) map.get(Sections.PHOTO_URL);
        String profileUrl = (String) map.get(Sections.PROFILE_URL);
        String memberId = (String) map.get(Sections.MEMBER_ID);





        Person person = new Person();

        person.setCity(getPersonCity((String) map.get(Sections.CITY_SELECTOR)));
        if (!fileUtil.isEmpty(name)) {
            Name personName = getPersonName(name);
            person.setName(personName);
        }
        if (!fileUtil.isEmpty(city))
            person.setCity(getPersonCity(city));
        if (!fileUtil.isEmpty(country))
            person.setCountry((String) map.get(Sections.COUNTRY_SELECTOR));
        if (!fileUtil.isEmpty(profileSummary))
            person.setProfileSummary(profileSummary);
        if (!fileUtil.isEmpty(role))
            person.setDesignation(role);
        if(!fileUtil.isEmpty(photoUrl)){
            if(photoUrl.length()<=200)
            person.setPhoto_url(photoUrl);
        }
        if (!fileUtil.isEmpty(educationFieldMap)) {
            List educationList = getPersonEducation(educationFieldMap);
            person.setEducation(educationList);
        }
        if (!fileUtil.isEmpty(experienceFieldMap)) {
            List experienceList = getPersonExperience(experienceFieldMap);
            person.setExperience(experienceList);
        }
        if (!fileUtil.isEmpty(projectFieldMap)) {
            List projectList = getPersonProject(projectFieldMap);
            person.setProjects(projectList);
        }

        Set websites=new HashSet();
        websites.add(website);
        if (!fileUtil.isEmpty(website))
            person.setWebsites(Lists.newArrayList(website));


        if (!fileUtil.isEmpty(interest)) {
            StringTokenizer st = new StringTokenizer(interest, ",");
            Set interests=new HashSet();
            while(st.hasMoreTokens())
                interests.add(st.nextToken());
            person.setInterests(interests);
        }

        Set<String> combinedSkills=new HashSet<String>();
        if(!fileUtil.isEmpty(skillSet))
            combinedSkills.addAll(skillSet);
        if(!fileUtil.isEmpty(topics))
            combinedSkills.addAll(topics);
        if(!fileUtil.isEmpty(specialization))
            combinedSkills.addAll(specialization);

        if(!fileUtil.isEmpty(combinedSkills))
            person.setSkills(Lists.newArrayList(combinedSkills));
      /*  if(!fileUtil.isEmpty(specialization))
            if(!fileUtil.isEmpty(specialization))
                person.setAreaOfSpecialization(Lists.newArrayList(specialization));
*/
       /* if(fileUtil.isEmpty(person.getSkills()) && !fileUtil.isEmpty(topics))
            person.setSkills(Lists.newArrayList(topics));
        if(fileUtil.isEmpty(person.getSkills()) && fileUtil.isEmpty(topics) && !fileUtil.isEmpty(specialization))
            person.setSkills(Lists.newArrayList(specialization));
*/
        if(!fileUtil.isEmpty(profileUrl))
            person.setProfileUrl(profileUrl);

        if(!fileUtil.isEmpty(memberId))
            person.setMemberId(memberId);

        person.setSource(CrawledSource.TECHGIG.name());
       // person.setCrawledDate(new Date());

        System.out.println("Person:"+person.toJSON());
        return person;
    }


    private static List getPersonProject(Set<Map<ProjectSection, Object>> projectFieldMap) {
        List<Projects> projectsList=new ArrayList();
        if (null != projectFieldMap) {
            for (Map<ProjectSection, Object> item : projectFieldMap) {
                Projects project=new Projects();
                String projectName = (String) item.get(ProjectSection.NAME);
                String projectDetail = (String) item.get(ProjectSection.DETAIL);
                String projectRole = (String) item.get(ProjectSection.ROLE);
                String roleDescription = (String) item.get(ProjectSection.KEY_RESPONSIBILITY);
                String duration = (String) item.get(ProjectSection.DURATION);
                String org=(String) item.get(ProjectSection.ORG);
                if (null != projectName && !projectName.isEmpty())
                    project.setName(projectName);
                if (null != org && !org.isEmpty())
                    project.setOrg(org);
                if (null != projectDetail && !projectDetail.isEmpty())
                    project.setDesc(projectDetail);
                if (null != projectRole && !projectRole.isEmpty())
                    project.setRole(projectRole);
                if (null != roleDescription && !roleDescription.isEmpty() && !"Information not supplied".equals(roleDescription))
                    project.setRoleDescription(roleDescription);
                if (null != duration && !duration.isEmpty()) {
                    try{
                        Map durationVal = durationSplitter(duration, " to ","project");

                        if (durationVal.size()>0) {
                            if (null != durationVal.get("start")) {
                                String[] startMonth = String.valueOf(durationVal.get("start")).split(",");
                                String startDate = (String) DateParser.monthMapper().get(startMonth[0].trim());
                                project.setStart(startDate +" "+ startMonth[1].replace(",", ""));
                            }
                            if (null != durationVal.get("end"))
                            if (durationVal.get("end").toString().contains("Present")) {
                                project.setEnd("Present");
                                }
                                else {
                                    String[] endMonth = String.valueOf(durationVal.get("end")).split(",");
                                    String endDate = (String) DateParser.monthMapper().get(endMonth[0].trim());
                                    project.setEnd(endDate +" "+ endMonth[1].replace(",", ""));

                                }
                        }

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }

                projectsList.add(project);

            }}
        return projectsList;
    }

    private static List getPersonExperience(Set<Map<ExperienceSection, Object>> experienceFieldMap) {
        List<Experience> experienceList=new ArrayList();
        if (null != experienceFieldMap) {
            int experienceCount=0;
            for (Map<ExperienceSection, Object> item : experienceFieldMap) {
                Experience experience=new Experience();
                experience.setResumeOrdinal(experienceCount);
                experienceCount=experienceCount+1;
                String company = (String) item.get(ExperienceSection.COMPANY);
                String designation = (String) item.get(ExperienceSection.DESIGNATION);
                String industry = (String) item.get(ExperienceSection.INDUSTRY);
                String duration = (String) item.get(ExperienceSection.DURATION);

                if (null != company && !company.isEmpty())
                    experience.setOrg(company);
                if (null != designation && !designation.isEmpty())
                    experience.setTitle(designation);
                if (null != duration && !duration.isEmpty()) {
                    try{
                        Map durationVal = durationSplitter(duration, " till ","job");
                        if (durationVal.size()>0) {
                            if (null != durationVal.get("start")) {
                                String[] startMonth = String.valueOf(durationVal.get("start")).split(" ");
                                String startDate = (String) DateParser.monthMapper().get(startMonth[0].trim());
                                experience.setStart(startDate +" "+ startMonth[1].replace(",", ""));
                            }
                            if (null != durationVal.get("end"))
                            if (durationVal.get("end").toString().contains("now")) {
                                    experience.setCurrent(true);
                                }
                                else {
                                    String[] endMonth = String.valueOf(durationVal.get("end")).split(" ");
                                    String endDate = (String) DateParser.monthMapper().get(endMonth[0].trim());
                                    experience.setEnd(endDate +" "+ endMonth[1].replace(",", ""));
                                }
                        }


                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }
                experienceList.add(experience);
            }
        }
        return experienceList;
    }

    private static List<Education> getPersonEducation(Set<Map<EducationSection, Object>> educationFieldMap) {
        List<Education> educationList = new ArrayList();
        if (null != educationFieldMap) {
            for (Map<EducationSection, Object> item : educationFieldMap) {
                Education education = new Education();
                String university = (String) item.get(EducationSection.UNIVERSITY);
                String degree = (String) item.get(EducationSection.DEGREE);
                String degreeLevel = (String) item.get(EducationSection.DEGREE_LEVEL);
                String duration = (String) item.get(EducationSection.DURATION);

                if (null != university && !university.isEmpty())
                    education.setName(university);
                if (null != degree && !degree.isEmpty())
                    education.setMajor(degree);
                if (null != degreeLevel && !degreeLevel.isEmpty())
                    education.setDegree(degreeLevel);
                if (null != duration && !duration.isEmpty()) {
                    try {
                        Map durationVal = durationSplitter(duration, " till ", "education");

                        if (durationVal.size() > 0) {
                            if (null != durationVal.get("start")) {
                                String[] startMonth = String.valueOf(durationVal.get("start")).split(" ");
                                String startDate = (String) DateParser.monthMapper().get(startMonth[0].trim());
                                education.setStart(startDate + " " + startMonth[1].replace(",", ""));
                            }
                            if (null != durationVal.get("end")) {
                                if (durationVal.get("end").toString().contains("now")) {
                                    education.setCurrent(true);
                                } else {
                                    String[] endMonth = String.valueOf(durationVal.get("end")).split(" ");
                                    String endDate = (String) DateParser.monthMapper().get(endMonth[0].trim());
                                    education.setEnd(endDate + " " + endMonth[1].replace(",", ""));
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                educationList.add(education);

            }
        }
        return educationList;
    }

    private static String getPersonCity(String city) {
        //for "Bengaluru / Bangalore~|~" split by "/" and get first word
        if(null!=city){
        if (city.contains("/")) {
            String[] cityStr = city.split("/");
            return cityStr[0];
        }
        //for "Kolkata~|~" replace the special characters
        else
            return city.replaceAll("[^a-zA-Z]+", "");}
        return null;
    }

    private static Name getPersonName(String name) {
        Name personName=new Name();

        if(!fileUtil.isEmpty(name)) {
            String[] names = name.split(" ", 2);
            personName.setGiven_name(names[0]);
            if(names.length>1)
                personName.setFamily_name(names[1]);
            return personName;
        }
        return null;
    }

    public static void main(String[] args) throws Exception {

        //http://www.techgig.com/manojbehera3
        //http://www.techgig.com/overview.php?uid=78957 - deactivated user
        FileUtil util=new FileUtil();
        String htmlSource=util.requestUrl(new URI("http://www.techgig.com/overview.php?uid=4"));
        TechGigParser parser=new TechGigParser();
        if(null!=htmlSource)
        parser.parsePerson(htmlSource);
        else
            System.out.println("user is deactivated");

    }
    public static String readFile(String localFile) throws Exception {
        FileReader fileReader = new FileReader( localFile );
        BufferedReader reader = new BufferedReader(fileReader);
        StringWriter stringWriter = new StringWriter();
        String line;
        while( ( line = reader.readLine() ) != null )
            stringWriter.write(line);
        return stringWriter.toString();
    }



}
