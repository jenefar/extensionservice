package com.piqube.marketplace.parser.sites.naukri;

import com.google.common.base.CharMatcher;
import com.piqube.apps.matcher.DictionaryMatcher;
import com.piqube.apps.vo.LocationData;
import com.piqube.marketplace.parser.Parser;
import com.piqube.marketplace.parser.ParsingException;
import com.piqube.model.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NaukriParser implements Parser{
	
	public static final String NAME_REGEX = "<title>(.*?)</title>";
	public static final String LANGUAGE_REGEX = "Languages Known</h2>(.*?)</table></div>";
	public static final String EDUCATION_REGEX = "<div class=\"section1 nSec\"><h2>Education</h2>(.*?)<a href=\"#dwnLnk\" class=\"txtUn f11 fr\">";
	public static final String WORK_EXPERIENCE_REGEX = "<div class=\"section nSec\"><h2>Work Experience</h2>(.*?)<a href=\"#dwnLnk\" class=\"txtUn f11 fr\">";
	public static final String IT_SKILLS_REGEX = "<div class=\"section cls nSec\"><h2>IT Skills</h2>(.*?)</div><a href=\"#dwnLnk\" class=\"txtUn f11 fr\">";
	public static final String PROJECT_REGEX = "<div class=\"section cls nSec\"><h2>Projects</h2>(.*?)</div></li><a href=\"#dwnLnk\" class=\"txtUn f11 fr\">";
	
	static Logger logger;

	private static void log(String string) {
		logger.info(string);
	}

	private static void logExp(String string, Exception exp) {
		logger.info(string, exp);
	}
	
	public static int countMatches(String str, String sub) {
		if (str.isEmpty() || sub.isEmpty()) {
			return 0;
		}
		int count = 0;
		int idx = 0;
		while ((idx = str.indexOf(sub, idx)) != -1) {
			count++;
			idx += sub.length();
		}
		return count;
	}

	public static List<String> filterSkill(String skillString) {

		List<String> fifthParseList = null;

		try {
			Pattern p = Pattern.compile("([(][^()]*[)])");
			Matcher m = p.matcher(skillString);
			List<String> firstParseList = new ArrayList<String>();
			while (m.find()) {
				firstParseList.add(m.group(0));
				skillString = skillString.replace(m.group(0), "");
			}
			firstParseList.add(skillString);
			List<String> thirdParseList = null;
			for (String strObj : firstParseList) {
				strObj = strObj.replace("(", "");
				strObj = strObj.replace(")", "");
				strObj = strObj.replace("&amp;", "");
				List<String> secondParseList = Arrays.asList(strObj
						.split("[,]"));
				thirdParseList = new ArrayList<String>();
				for (String strObjInner : secondParseList) {
					if (countMatches(strObjInner, "/") > 1) {
						thirdParseList.addAll(Arrays.asList(strObjInner
								.replace("/", "|").split("[|]")));
					} else {
						thirdParseList.add(strObjInner.trim());
					}
				}
			}
			List<String> fourthParseList = new ArrayList<String>();
			for (String strObjInner : thirdParseList) {
				strObjInner = CharMatcher.WHITESPACE.trimFrom(strObjInner);
				fourthParseList.add(strObjInner);
			}
			fifthParseList = new ArrayList<String>();
			for (String strObjInner : fourthParseList) {
				if (strObjInner.contains("~")) {
					String[] tildeBreakArray = strObjInner.split("[~]");
					for (String strObjNextInner : tildeBreakArray) {
						strObjNextInner = CharMatcher.WHITESPACE
								.trimFrom(strObjNextInner);
						fifthParseList.add(strObjNextInner);
					}
				} else {
					strObjInner = CharMatcher.WHITESPACE.trimFrom(strObjInner);
					fifthParseList.add(strObjInner);
				}
			}
		} catch (Exception e) {
			logExp("Ignorable Exception while parsing skills", e); // To change
																	// body of
																	// catch
																	// statement
																	// use File
																	// |
																	// Settings
																	// | File
																	// Templates.
		}
		return fifthParseList;
	}

	public List<Education> parseEducation(String htmlSource)
			throws ParsingException {
		List<Education> educationList = new ArrayList<Education>();
		Matcher matcher = Pattern.compile(EDUCATION_REGEX).matcher(htmlSource);

		if (!matcher.find())
			throw new ParsingException();
		String educationHtmlSource = matcher.group(1);

		Document summaryDOC = Jsoup.parse(educationHtmlSource);
		Elements elements = summaryDOC.select("li");
		if (elements != null) {
			for (Element element : elements) {
				if (element.text().startsWith("UG:")
						|| element.text().startsWith("PG:")) {
					Pattern pattern = Pattern
							.compile("(.*?)from\\s(.*?)in\\s(.*?)$");
					Matcher match = pattern.matcher(element.text());
					Education education = new Education();

					String[] degreeFos = null;
					if (match.find()) {
						if (match.group(1).contains("(")) {
							degreeFos = match.group(1).trim().split("[(]");
							education.setDegree(CharMatcher.WHITESPACE
									.trimFrom(degreeFos[0]
											.substring(degreeFos[0]
													.lastIndexOf(":") + 1)));
							// Education.setDegreeLevel(CharMatcher.WHITESPACE.trimFrom(degreeFos[0].substring(0,
							// degreeFos[0].indexOf(":"))));
							// System.out.println("Degree :" +
							// Education.getDegree());
						} else {
							education.setDegree(CharMatcher.WHITESPACE
									.trimFrom(match.group(1)
											.substring(
													match.group(1).lastIndexOf(
															":") + 1)));
							// Education.setDegreeLevel(CharMatcher.WHITESPACE.trimFrom(match.group(1).substring(0,
							// match.group(1).indexOf(":"))));
						}

						if (degreeFos.length > 1) {
							education.setMajor(degreeFos[1].replaceAll("[)]",
									""));
						}

						education.setName(CharMatcher.WHITESPACE.trimFrom(match
								.group(2)));
						education.setEnd(CharMatcher.WHITESPACE.trimFrom(match
								.group(3)));
						educationList.add(education);

					}

				}

			}
		}
		return educationList;
	}

	public List<Experience> parseExperience(String htmlSource)
			throws ParsingException {
		ArrayList<Experience> experienceList = new ArrayList<Experience>();
		Matcher matcher = Pattern.compile(WORK_EXPERIENCE_REGEX).matcher(
				htmlSource.toString());
		if (!matcher.find()) {
			throw new ParsingException("Experience Not Found");
		}
		String experienceHtmlSource = matcher.group(1);

		Document doc = Jsoup.parse(experienceHtmlSource);
		Elements links = doc.select("li");

		for (int i = 0; i < links.size(); i++) {
			Experience experience = new Experience();

			try {
				String companyName = null;
				if (links.get(i).getElementsByClass("f14").size() > 0
						&& null != links.get(i).getElementsByClass("f14")
								.get(0).text()
						&& !(links.get(i).getElementsByClass("f14").get(0)
								.text().isEmpty())) {
					companyName = links.get(i).getElementsByClass("f14").get(0)
							.text();
					experience.setOrg(companyName);
				}

				String role = null;
				if (links.get(i).getElementsByClass("f14").size() > 1
						&& null != links.get(i).getElementsByClass("f14")
								.get(1).text()
						&& !(links.get(i).getElementsByClass("f14").get(1)
								.text().isEmpty())) {
					role = links.get(i).getElementsByClass("f14").get(1).text();
					experience.setTitle(role);
				}

				String timePeriod = null;
				if (links.get(i).getElementsByClass("f14").size() > 2
						&& null != links.get(i).getElementsByClass("f14")
								.get(2).text()
						&& !links.get(i).getElementsByClass("f14").get(2)
								.text().equals("N/A")) {
					timePeriod = links.get(i).getElementsByClass("f14").get(2)
							.text();

					experience.setStart(timePeriod.split("to")[0].trim());

					if (timePeriod.length() > 1
							&& !timePeriod.split("to")[1].trim()
									.equalsIgnoreCase("Till Date")
							&& timePeriod.split("to")[1] != "N/A") {
						experience.setEnd(timePeriod.split("to")[1].trim());
					}
				}
				Elements desc = links.get(i).select(
						"span[class=cls mt10 dspB blktxtimp]");

				if (desc.size() > 0) {
					if (!desc.get(0).text().isEmpty() && null != desc.get(0)) {
						experience.setDesc(desc.get(0).text());
					}
				}

				
				
				experienceList.add(experience);

			} catch (Exception e) {
				logExp("Ignorable Exception while parsing experience", e);
			}
		}

		return experienceList;
	}

	public Person parsePerson(String htmlSource) throws ParsingException {
		Matcher matcher = Pattern.compile(NAME_REGEX).matcher(
				htmlSource.toString());
		String nameMatch = null;
		while (matcher.find()) {
			nameMatch = matcher.group(1);
		}
		if (nameMatch == null)
			throw new ParsingException("Unable to Parse Name");

		String text[] = nameMatch.split("-");
		String names[] = text[0].split("\\ ", 2);

		Person p = new Person();

		p.setSource("NAUKRI");
		Name name = new Name();
		name.setGiven_name(names[0].trim());
		name.setFamily_name(names[1].trim());

		p.setName(name);

		p.setIngestedDate(new Date());

		Document doc = Jsoup.parse(htmlSource);
		// Source Id
		Element idSectionElement = doc.getElementById("actInfo");
		Elements idSectionSubElements = idSectionElement.getElementsByTag("i");

		String naukriId = idSectionSubElements.first().getAllElements().first()
				.text().split(":")[1].trim();
		p.setMemberId(naukriId);

		// Summary
		Elements summaryElements = doc.select("div.section p");
		if (summaryElements.size() > 0 && null != summaryElements.get(0)) {
			p.setSummary(summaryElements.get(0).text());
		}
		// Languages Known
		matcher = Pattern.compile(LANGUAGE_REGEX).matcher(htmlSource);

		if (matcher.find()) {
			List<Language> languages = new ArrayList<Language>();
			Document docLanguage = Jsoup.parse(matcher.group(1));
			Element table = docLanguage.select("table[class=tableDMNR]")
					.first();
			Elements trElements = table.select("tr");
			Set<String> set = new HashSet<String>();
			for (Element tdElement : trElements) {
				if (!tdElement.text().startsWith("Language")) {
					set.add(tdElement.text());
				}
			}
			List<String> languageList = new ArrayList<String>(set);
			for (String s : languageList) {
				String arr[] = s.split(" ");
				s = Arrays.asList(arr).get(0) + "/" + Arrays.asList(arr).get(1);
				Language lang = new Language();
				lang.setName(Arrays.asList(arr).get(0));
				lang.setProficiency(Arrays.asList(arr).get(1));
				languages.add(lang);

			}
			p.setLanguages_known(languages);
		}

		// Basic profile data
		Elements links = doc.select("ul li");
		Elements profileTitleElement = doc.select("div[class=cvPrev zm1] p");

		p.setTagline(profileTitleElement.get(0).text());
		for (Element element : links) {

			if (element.text().contains("Current Designation:")
					&& !element.text().isEmpty()) {
				p.setDesignation(CharMatcher.WHITESPACE.trimFrom(element.text()
						.substring(element.text().lastIndexOf(":") + 1)));
			}

			if (element.text().contains("Industry:") && element.text() != null
					&& !element.text().isEmpty()) {
				p.setIndustry(CharMatcher.WHITESPACE.trimFrom(element.text()
						.substring(element.text().lastIndexOf(":") + 1)));
			}

			if (element.text().contains("Pref. Location:")
					&& element.text() != null && !element.text().isEmpty()) {
				String prefLocationsString = CharMatcher.WHITESPACE
						.trimFrom(element.text().substring(
								element.text().lastIndexOf(":") + 1));
				ArrayList<String> prefLocations = new ArrayList<String>();

				String[] prefLocationsArray = prefLocationsString.split(",");

				if (prefLocationsArray != null)
					for (int i = 0; i < prefLocationsArray.length; i++)
						prefLocations.add(prefLocationsArray[i]);

				p.setPreferred_locations(prefLocations);

			}

			if (element.text().contains("Current Location:")
					&& element.text() != null && !element.text().isEmpty()) {
				String curLocationsString = CharMatcher.WHITESPACE
						.trimFrom(element.text().substring(
								element.text().lastIndexOf(":") + 1));
				LocationData locData = DictionaryMatcher
						.getNormalizedLocation(curLocationsString);
				p.setCity(locData.sCity);
				p.setState(locData.sState);
				p.setCountry(locData.sCountry);
				p.setCurrentLocation(locData.sCountry);
				p.setLocality(locData.sCity);
			}

			// address
			if (element.text().contains("Address:") && !isEmpty(element.text())) {
				String address = (CharMatcher.WHITESPACE.trimFrom(element
						.text().substring(element.text().lastIndexOf(":") + 1)));
				LocationData locData = null;
				try {
					locData = DictionaryMatcher
							.getNormalizedLocation(address);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				if (isEmpty(p.getCity()) && null!=locData)
					p.setCity(locData.sCity);
				if (isEmpty(p.getCountry())&& null!=locData)
					p.setCountry(locData.sCountry);
				if (isEmpty(p.getCurrentLocation())&& null!=locData)
					p.setCurrentLocation(locData.sCountry);
				if (isEmpty(p.getCity())&& null!=locData)
					p.setLocality(locData.sCity);
				if (isEmpty(p.getState())&& null!=locData)
					p.setState(locData.sState);
			}

			if (element.text().contains("Email:") && !element.text().isEmpty()) {
				final String[] array = element.text()
						.substring(element.text().lastIndexOf(":") + 1)
						.split("Verified");
				p.setEmails(new ArrayList<String>() {
					{
						add(CharMatcher.WHITESPACE.trimFrom(array[0]));
					}
				});
			}

			if (element.text().contains("Key Skills:")
					&& !element.text().isEmpty()) {
				List<String> skills = filterSkill(element.text().substring(
						element.text().lastIndexOf(":") + 1));
				p.setSkills(skills);
			}

			List<Certifications> certificationList = new ArrayList<Certifications>();
			if (element.text().contains(
					"Other Qualifications/Certifications/Programs:")) {
				// System.out.println("certifiactionE " + element.html());
				String certificationString = element.html().substring(
						element.html().indexOf(":") + 1);
				String[] certArray = certificationString.split("<br />");
				for (int m = 0; m < certArray.length; m++) {
					String certificate = certArray[m].replaceAll("<(.*)>", "");
					if (null != certificate && !certificate.trim().isEmpty()) {
						// LOGGER.debug(" The certificate coming as " +
						// certificate);
						Certifications certification = new Certifications();
						certification.setName(certificate);
						certificationList.add(certification);
					}
				}

				p.setCertifications(certificationList);
			}
		}
		// IT Skills
		matcher = Pattern.compile(IT_SKILLS_REGEX).matcher(htmlSource);
		List<String> naukriSkillList = new ArrayList<String>();
		if (matcher.find()) {
			Document docITSkills = Jsoup.parse(matcher.group(1));
			Element table = docITSkills.select("table[class=tableDMNR]")
					.first();
			Elements trElements = table.select("tr");
			for (int i = 0; i < trElements.size(); i++) {
				if (!trElements.get(i).text().startsWith("Skill Name")) {

					if (null != trElements.get(i).children().get(0).text()) {
						if (trElements.get(i).children().get(0).text()
								.contains(",")) {
							String[] skillsArray = trElements.get(i).children()
									.get(0).text().split(",");
							for (int k = 0; k < skillsArray.length; k++) {
								String naukriSkill = CharMatcher.WHITESPACE
										.trimFrom(skillsArray[k]);
								naukriSkillList.add(naukriSkill);
							}
						} else {
							String naukriSkill = trElements.get(i).children()
									.get(0).text();
							naukriSkillList.add(naukriSkill);
						}
					}
				}
			}
			p.getSkills().addAll(naukriSkillList);
		}
		// Project

		List<Projects> naukriProjectList = parseProjectList(htmlSource);
		if(null !=naukriProjectList)
			p.setProjects(naukriProjectList);

		// Address

		// Education
		List<Education> educationList = parseEducation(htmlSource);

		if(null !=educationList)
			p.setEducation(educationList);

		// Experience
		List<Experience> experienceList = parseExperience(htmlSource);
		if(null !=experienceList)
			p.setExperience(experienceList);

		// Photo
		Elements links1 = doc.select("img[class=jseekImg");
		String photoSrc = null;
		for (Element link : links1) {
			photoSrc = link.getAllElements().attr("src");
		}

		if (!photoSrc.contains("static.naukimg.com"))
			p.setPhoto_url(photoSrc);

		return p;

	}

	private List<Projects> parseProjectList(String htmlSource) {
		ArrayList<Projects> projects = new ArrayList<Projects>();
		Matcher matcher = Pattern.compile(PROJECT_REGEX).matcher(
				htmlSource.toString());
		if (!matcher.find())
			return null;
		String outputHtml = matcher.group(1);

		if (null != outputHtml) {
			Document doc = Jsoup.parse(outputHtml);
			Elements proElements = doc.select("div[class=tup]");
			if (null != proElements) {
				for (Element element : proElements) {
					Elements spanElements = element.getElementsByTag("span");
					Projects naukriProject = new Projects();
					try {
						for (Element span : spanElements) {
							if (span.text().contains("Project Title:")
									&& !isEmpty(span.text())) {
								naukriProject.setName(CharMatcher.WHITESPACE
										.trimFrom(span.text().substring(
												span.text().indexOf(":") + 1)));
							}
							if (span.text().contains("Client:")
									&& !isEmpty(span.text())) {
								naukriProject
										.setClient(span
												.text()
												.substring(
														span.text()
																.indexOf(":") + 1)
												.trim());
							}
							if (span.text().contains("Nature of Employment:")
									&& !isEmpty(span.text())) {
								if (CharMatcher.WHITESPACE
										.trimFrom(span.text().substring(
												span.text().indexOf(":") + 1)) == "Full Time"
										|| CharMatcher.WHITESPACE
												.trimFrom(
														span.text()
																.substring(
																		span.text()
																				.indexOf(
																						":") + 1))
												.equals("Full Time")) {
									naukriProject
											.setEmploymentNature("FULLTIME");
								} else {
									naukriProject
											.setEmploymentNature("PARTTIME");
								}
							}
							if (span.text().contains("Project Location:")
									&& !isEmpty(span.text())) {
								naukriProject
										.setLocation(CharMatcher.WHITESPACE
												.trimFrom(span
														.text()
														.substring(
																span.text()
																		.indexOf(
																				":") + 1)));
							}
							if (span.text().contains("Role:")
									&& !isEmpty(span.text())) {
								naukriProject.setRole(CharMatcher.WHITESPACE
										.trimFrom(span.text().substring(
												span.text().indexOf(":") + 1)));
							}
							if (span.text().contains("Duration:")
									&& !isEmpty(span.text())) {
								String date = span.text().substring(
										span.text().indexOf(":") + 1);
								naukriProject.setStart(CharMatcher.WHITESPACE
										.trimFrom(date.split("-")[0]));
								if (date.split("-")[1].trim() != "Till date"
										|| date.split("-")[1].trim() != "N/A") {
									naukriProject.setEnd(CharMatcher.WHITESPACE
											.trimFrom(date.split("-")[1]));
								}
							}
							if (span.text().contains("Onsite / Offsite:")
									&& !isEmpty(span.text())) {
								if (CharMatcher.WHITESPACE
										.trimFrom(span.text().substring(
												span.text().indexOf(":") + 1)) == "Onsite"
										|| CharMatcher.WHITESPACE
												.trimFrom(
														span.text()
																.substring(
																		span.text()
																				.indexOf(
																						":") + 1))
												.equals("Onsite")) {
									naukriProject.setShoreType("ONSITE");
								}
								if (CharMatcher.WHITESPACE
										.trimFrom(span.text().substring(
												span.text().indexOf(":") + 1)) == "Offsite"
										|| CharMatcher.WHITESPACE
												.trimFrom(
														span.text()
																.substring(
																		span.text()
																				.indexOf(
																						":") + 1))
												.equals("Offsite")) {
									naukriProject.setShoreType("OFFSHORE");
								}
							}
							if (span.text().contains("Team Size:")
									&& !isEmpty(span.text())) {
								String size = CharMatcher.WHITESPACE
										.trimFrom(span.text().substring(
												span.text().indexOf(":") + 1));
								naukriProject
										.setTeamSize(size);
							}
							if (span.text().contains("Skill Used:")
									&& !isEmpty(span.text())) {
								String[] skills = CharMatcher.WHITESPACE
										.trimFrom(
												span.text()
														.substring(
																span.text()
																		.indexOf(
																				":") + 1))
										.split(",");
								List<String> projectSkillList = new ArrayList<String>();
								for (int i = 0; i < skills.length; i++) {
									projectSkillList.add(skills[i].trim());
								}
								naukriProject.setSkills(projectSkillList);
							}
							if (span.text().contains("Role Description:")
									&& !isEmpty(span.text())) {
								naukriProject
										.setRoleDescription(CharMatcher.WHITESPACE
												.trimFrom(span
														.text()
														.substring(
																span.text()
																		.indexOf(
																				":") + 1)));
							}
							if (span.text().contains("Project Details:")
									&& !isEmpty(span.text())) {
								naukriProject.setDesc(CharMatcher.WHITESPACE
										.trimFrom(span.text().substring(
												span.text().indexOf(":") + 1)));
							}
						}
						projects.add(naukriProject);
					} catch (Exception e) {
						log("Exception: " + e);
					}
				}
			}
		}

		return projects;

	}

	public boolean isEmpty(String string) {
		if (null == string || 0 == string.trim().length()) {
			return true;
		}
		if (string.contains("N/A")) {
			return true;
		}
		return false;
	}

}
