package com.piqube.marketplace.parser.sites.indeed;

import com.piqube.apps.matcher.DictionaryMatcher;
import com.piqube.apps.vo.LocationData;
import com.piqube.marketplace.parser.IEnumValue;
import com.piqube.marketplace.parser.ParsingException;
import com.piqube.marketplace.parser.parserutils.Arg;
import com.piqube.marketplace.parser.parserutils.HtmlParseUtils;
import com.piqube.marketplace.parser.parserutils._SelectorMetaInfo;
import com.piqube.marketplace.parser.utils.FileUtil;
import com.piqube.model.Education;
import com.piqube.model.Experience;
import com.piqube.model.Name;
import com.piqube.model.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URI;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Anuroop on 29-06-2015.
 */
public class IndeedParser implements com.piqube.marketplace.parser.Parser {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(IndeedParser.class);
	static FileUtil fileUtil = new FileUtil();

	public static enum Sections implements IEnumValue<_SelectorMetaInfo> {

		NAME_SELECTOR(new _SelectorMetaInfo(
				com.piqube.marketplace.parser.parserutils.Arg.E("#basic_info_cell h1"))), TAGLINE_SELECTOR(
				new _SelectorMetaInfo(
						com.piqube.marketplace.parser.parserutils.Arg
								.E("#basic_info_cell h2"))), CURRENTLOCATION_SELECTOR(
				new _SelectorMetaInfo(
						com.piqube.marketplace.parser.parserutils.Arg
								.E("#basic_info_cell #headline_location"))), SUMMARY_SELECTOR(
				new _SelectorMetaInfo(
						com.piqube.marketplace.parser.parserutils.Arg
								.E("#basic_info_cell #res_summary"))), EXPERIENCE_SELECTOR(
				new _SelectorMetaInfo(
						com.piqube.marketplace.parser.parserutils.Arg
								.E("#work-experience-items .work-experience-section"),
						Arg.list(), Arg.Children(ExperienceSection.class))), EDUCATION_SELECTOR(
				new _SelectorMetaInfo(
						com.piqube.marketplace.parser.parserutils.Arg
								.E("#education-items .education-section"),
						Arg.list(), Arg.Children(EducationSection.class))), SKILLS_SELECTOR(
				new _SelectorMetaInfo(
						com.piqube.marketplace.parser.parserutils.Arg.E("#skills-items "))), LINKS_SELECTOR(
				new _SelectorMetaInfo(
						com.piqube.marketplace.parser.parserutils.Arg.E("#link-items a"))), ADDINFO_SELECTOR(
				new _SelectorMetaInfo(
						com.piqube.marketplace.parser.parserutils.Arg
								.E("#additionalinfo-items  .data_display"))), ;
		private _SelectorMetaInfo metaInfo;

		private Sections(_SelectorMetaInfo info) {
			this.metaInfo = info;
		}

		public _SelectorMetaInfo get() {
			return metaInfo;
		}

	}

	public static final String MEMBERID_REGEX = "<link rel=\"canonical\" href=\"(.*?)\"/>";

	public static enum ExperienceSection implements
			IEnumValue<_SelectorMetaInfo> {
		DESIGNATION(new _SelectorMetaInfo(Arg.E(".data_display .work_title"))), COMPANY(
				new _SelectorMetaInfo(Arg.E(".data_display .bold"))), LOCATION(
				new _SelectorMetaInfo(Arg.E(" .data_display .inline-block"))), DESCRIPTION(
				new _SelectorMetaInfo(Arg.E(" .data_display .work_description"))), DURATION(
				new _SelectorMetaInfo(Arg.E(".data_display .work_dates"))), ;
		public _SelectorMetaInfo metaInfo;

		private ExperienceSection(_SelectorMetaInfo metaInfo) {
			this.metaInfo = metaInfo;
		}

		public _SelectorMetaInfo get() {
			return metaInfo;
		}
	}

	public enum EducationSection implements IEnumValue<_SelectorMetaInfo> {

		DEGREE(new _SelectorMetaInfo(Arg.E(".data_display .edu_title"))), UNIVERSITY(
				new _SelectorMetaInfo(Arg.E(".data_display .bold"))), LOCATION(
				new _SelectorMetaInfo(Arg.E(" .data_display .inline-block"))), DURATION(
				new _SelectorMetaInfo(Arg.E(".data_display .edu_dates"))),

		;
		public _SelectorMetaInfo metaInfo;

		private EducationSection(_SelectorMetaInfo metaInfo) {
			this.metaInfo = metaInfo;
		}

		public _SelectorMetaInfo get() {
			return metaInfo;
		}
	}

	int num = 1;

	public Person parsePerson(String htmlsource) throws ParsingException {
		Map<Sections, Object> map = HtmlParseUtils.parse(htmlsource,
				Sections.class);
		Set<Map.Entry<Sections, Object>> entries = map.entrySet();
		/*
		 * for (Map.Entry<Sections, Object> entry : entries) {
		 * System.out.println(entry.getKey());
		 * System.out.println(entry.getValue()); }
		 */

		Matcher matcher = Pattern.compile(MEMBERID_REGEX).matcher(
				htmlsource.toString());
		String memberidMatch = null;
		while (matcher.find()) {
			memberidMatch = matcher.group(1);
		}
		if (memberidMatch == null)
			throw new ParsingException("Unable to Parse memberid");

		String name = (String) map.get(Sections.NAME_SELECTOR);
		String role = (String) map.get(Sections.TAGLINE_SELECTOR);
		String currentloc = (String) map.get(Sections.CURRENTLOCATION_SELECTOR);
		String summary = (String) map.get(Sections.SUMMARY_SELECTOR);
		String links = (String) map.get(Sections.LINKS_SELECTOR);
		String addinfo = (String) map.get(Sections.ADDINFO_SELECTOR);

		Set<Map<ExperienceSection, Object>> experienceFieldMap = 
				(Set<Map<ExperienceSection, Object>>) map.get(Sections.EXPERIENCE_SELECTOR);
		Set<Map<EducationSection, Object>> educationFieldMap = 
				(Set<Map<EducationSection, Object>>) map.get(Sections.EDUCATION_SELECTOR);
		String skills = (String) map.get(Sections.SKILLS_SELECTOR);

		if (fileUtil.isEmpty(skills)) {

			try {
				File file = new File("C:\\rejected\\file" + num + ".html");
				if (!file.exists()) {
					file.createNewFile();
				}
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(htmlsource);
				bw.close();
				num++;
			} catch (IOException e) {
				e.printStackTrace();
			}
			throw new ParsingException("Skills are empty");
		}

		Person person = new Person();

		String[] Skill = skills.split(",");
		String source = "INDEED";
		person.setCity(getPersonCity((String) map
				.get(Sections.CURRENTLOCATION_SELECTOR)));
		if (!fileUtil.isEmpty(name)) {
			Name personName = getPersonName(name);
			person.setName(personName);

		}
		LocationData locData = null;
		if (!fileUtil.isEmpty(currentloc))
			locData = DictionaryMatcher.getNormalizedLocation(currentloc);

		person.setCity(locData.sCity);
		person.setState(locData.sState);
		person.setCountry(locData.sCountry);
		person.setCurrentLocation(locData.sCountry);
		person.setLocality(locData.sCity);
		if (!fileUtil.isEmpty(summary))
			person.setProfileSummary(summary);
		if (!fileUtil.isEmpty(role))
			person.setDesignation(role);
		if (!fileUtil.isEmpty(skills))
			person.setSkills((new ArrayList(Arrays.asList(Skill))));
		if (!fileUtil.isEmpty(links))
			person.setWebsites(new ArrayList(Arrays.asList(links)));

		if (!fileUtil.isEmpty(educationFieldMap)) {
			List educationList = getPersonEducation(educationFieldMap);
			person.setEducation(educationList);
		}
		if (!fileUtil.isEmpty(experienceFieldMap)) {
			List experienceList = getPersonExperience(experienceFieldMap);
			person.setExperience(experienceList);
		}
		if (!fileUtil.isEmpty(addinfo))
			person.setAddInfo(addinfo);

		person.setSource(source);
		person.setMemberId(memberidMatch);
		System.out.println("Person:" + person.toJSON());
		return person;
	}

	private static List getPersonExperience(
			Set<Map<ExperienceSection, Object>> experienceFieldMap) {
		List<Experience> experienceList = new ArrayList();
		if (null != experienceFieldMap) {
			int experienceCount = 0;
			for (Map<ExperienceSection, Object> item : experienceFieldMap) {
				Experience experience = new Experience();

				experience.setResumeOrdinal(experienceCount);
				experienceCount = experienceCount + 1;
				String company = (String) item.get(ExperienceSection.COMPANY);
				String designation = (String) item
						.get(ExperienceSection.DESIGNATION);
				String location = (String) item.get(ExperienceSection.LOCATION);
				String description = (String) item
						.get(ExperienceSection.DESCRIPTION);
				String duration = (String) item.get(ExperienceSection.DURATION);

				if (null != company && !company.isEmpty())
					experience.setOrg(company);
				if (null != designation && !designation.isEmpty())
					experience.setTitle(designation);
				if (null != location && !location.isEmpty())
					experience.setLocation(location);
				if (null != description && !description.isEmpty())
					experience.setDesc(description);
				if (null != duration && !duration.isEmpty()) {
					String[] timeperiod = duration.split(" to ");

					if (timeperiod.length > 1) {
						experience.setStart(timeperiod[0].trim());
						experience.setEnd(timeperiod[1].trim());
					} else if (timeperiod.length >= 1) {
						experience.setStart(null);
						experience.setEnd(timeperiod[0].trim());
					} else {
						experience.setStart(null);
						experience.setEnd(null);
					}

				}
				experienceList.add(experience);
			}
		}
		return experienceList;
	}

	private static List<Education> getPersonEducation(
			Set<Map<EducationSection, Object>> educationFieldMap) {
		List<Education> educationList = new ArrayList();
		if (null != educationFieldMap) {

			for (Map<EducationSection, Object> item : educationFieldMap) {
				Education education = new Education();

				String degree = (String) item.get(EducationSection.DEGREE);
				String university = (String) item
						.get(EducationSection.UNIVERSITY);
				String location = (String) item.get(EducationSection.LOCATION);
				String duration = (String) item.get(EducationSection.DURATION);

				if (null != university && !university.isEmpty())
					education.setName(university);
				if (null != degree && !degree.isEmpty())
					education.setDegree(degree);
				if (null != location && !location.isEmpty())
					education.setLocation(location);
				if (null != duration && !duration.isEmpty()) {
					String[] timeperiod = duration.split(" to ");

					if (timeperiod.length > 1) {
						education.setStart(timeperiod[0].trim());
						education.setEnd(timeperiod[1].trim());
					} else if (timeperiod.length >= 1) {
						education.setStart(null);
						education.setEnd(timeperiod[0].trim());
					} else {
						education.setStart(null);
						education.setEnd(null);
					}

				}
				educationList.add(education);
			}
		}
		return educationList;
	}

	private static Name getPersonName(String name) {
		Name personName = new Name();

		if (!fileUtil.isEmpty(name)) {
			String[] names = name.split(" ");
			personName.setGiven_name(names[0]);
			if (names.length > 1)
				personName.setFamily_name(names[1]);
			return personName;
		}
		return null;
	}

	private static String getPersonCity(String city) {

		if (!fileUtil.isEmpty(city)) {
			String[] location = city.split(",");
			String City = location[0];
			return City;
		}
		return null;

	}

	private static String getPersonState(String state) {

		if (!fileUtil.isEmpty(state)) {
			String[] location = state.split(",");
			String State = location[1];
			return State;
		}
		return null;

	}

	public static void main(String[] args) throws Exception {
		FileUtil util = new FileUtil();
		String htmlSource = util.requestUrl(new URI(
				"http://www.indeed.com/r/Pramod-Mishra/853009707bf1fb2a?sp=0"));
		IndeedParser parser = new IndeedParser();
		System.out.println(htmlSource);
		if (null != htmlSource)
			parser.parsePerson(htmlSource);
		else
			System.out.println("user is deactivated");
	}

	public static String readFile(String localFile) throws Exception {
		FileReader fileReader = new FileReader(localFile);
		BufferedReader reader = new BufferedReader(fileReader);
		StringWriter stringWriter = new StringWriter();
		String line;
		while ((line = reader.readLine()) != null)
			stringWriter.write(line);
		return stringWriter.toString();
	}

}
