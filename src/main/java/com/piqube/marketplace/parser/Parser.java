package com.piqube.marketplace.parser;

import com.piqube.model.Person;

public interface Parser {
	
public Person parsePerson(String htmlSource) throws ParsingException;

}
