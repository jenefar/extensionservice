package com.piqube.marketplace.parser.parserutils.parsehelpers;

import com.piqube.marketplace.parser.utils.UnaryFunction;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ranjan on 22/4/14.
 */
public class RegexFetcher implements UnaryFunction<String,String> {

    public String patternVar;
    public String patternStr;
    public Pattern pattern;

    public RegexFetcher(String patternVar, String patternStr) {
        this.patternVar = patternVar;
        this.patternStr = patternStr;
        this.pattern = Pattern.compile(this.patternStr);
    }

    @Override
    public String exec(String input) {
        Matcher matcher = pattern.matcher(input);
        if ( matcher.find() ) {
            return matcher.group(this.patternVar);
        }
        return null;
    }
}
