package com.piqube.marketplace.parser.parserutils;


import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;
import com.piqube.marketplace.parser.IEnumValue;
import com.piqube.marketplace.parser.utils.UnaryFunction;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class JsonParseUtils {

    public static boolean isBasicType(Object obj) {
        return (   obj instanceof Integer || obj instanceof String || obj instanceof Double
                || obj instanceof Float || obj instanceof Boolean );
    }

    public static Object parseElem(Object matchedBlob, _SelectorMetaInfo metaInfo) {
        UnaryFunction postFunc = metaInfo.postFunc;
        if (isBasicType(matchedBlob)) {
            String matchedStr = matchedBlob.toString();
            if (postFunc != null)
                return postFunc.exec(matchedStr);
            else
                return matchedStr;
        } else if (matchedBlob instanceof JSONArray) {
            JSONArray matchedElems = (JSONArray)matchedBlob;
            if (matchedElems != null && matchedElems.size() > 0) {
                Object matchedObj = matchedElems.get(0);
                String matchedStr = null;
                if(matchedObj != null)
                  matchedStr = matchedObj.toString();

                if (postFunc != null)
                    return postFunc.exec(matchedStr);
                else
                    return matchedStr;
            }
        }
        return null;
    }

    public static Object parseList(Object matchedBlob, _SelectorMetaInfo metaInfo) {
        UnaryFunction postFunc = metaInfo.postFunc;
        JSONArray matchedElems = null;
        if (matchedBlob instanceof JSONObject || isBasicType(matchedBlob) ) {
            JSONArray jsonArray = new JSONArray();
            jsonArray.add(matchedBlob);
            matchedElems = jsonArray;
        } else {
            matchedElems = (JSONArray)matchedBlob;
        }

        if (matchedElems != null && matchedElems.size() > 0) {
            Set<Object> matchedList = new HashSet<Object>();
            Object matchedObj = null;
            for (Object matchedElem : matchedElems) {
                if (metaInfo.children != null && metaInfo.children.size() > 0) {
                    matchedObj = parseStruct(matchedElem,metaInfo);
                } else
                    matchedObj = matchedElem;

                String matchedStr = matchedElem.toString();
                if (postFunc != null)
                    matchedObj = postFunc.exec(matchedStr);
                matchedList.add(matchedObj);
            }
            return matchedList;
        }
        return null;
    }

    public static <T extends Enum<T> & IEnumValue<_SelectorMetaInfo>>
    Map<T,Object> parseStruct(Object element, _SelectorMetaInfo metaInfo)
    {
        if ( ! ( element instanceof JSONObject ) )
            return null;
        JSONObject matchedObj = (JSONObject) element;
        HashMap<T,Object> matchedMap = new HashMap<>();
        for( Map.Entry<T,_SelectorMetaInfo> childMetaInfoEntry :
                ((Map<T,_SelectorMetaInfo>) metaInfo.children).entrySet() ) {
            T childSelectorName = childMetaInfoEntry.getKey();
            _SelectorMetaInfo childSelector = childMetaInfoEntry.getValue();
            Object childResult = parse(JsonPath.parse(matchedObj), childSelector);
            if ( childResult != null )
                matchedMap.put( childSelectorName, childResult );
        }
        return matchedMap.isEmpty() ? null : matchedMap;

    }

    public static Object parse( ReadContext pageCtx , _SelectorMetaInfo metaInfo ) {

        String selectorStr = metaInfo.selector;
        _SelectorMetaInfo.SelectorType selectorType = metaInfo.selectorType;

        Object matchedBlob;
        try {
            matchedBlob = pageCtx.read(selectorStr);
        } catch(Exception ex) {
            return null;
        }

        switch(selectorType) {
            case STRING: {
                return parseElem(matchedBlob,metaInfo);
            }
            case LIST: {
                return parseList(matchedBlob,metaInfo);
            }
            case STRUCT: {
                return parseStruct(matchedBlob,metaInfo);
            }
        }
        return null;
    }

    public static Object parse( String pageStr, _SelectorMetaInfo metaInfo ) {
        ReadContext pageCtx = JsonPath.parse(pageStr);
        return parse(pageCtx, metaInfo);
    }

    public static <T extends Enum<T> & IEnumValue<_SelectorMetaInfo>>
    Map<T,Object> parse( String page, HashMap<T,_SelectorMetaInfo> metaInfoMap )
    {

        HashMap<T, Object> results = new HashMap<T, Object>();
        ReadContext pageCtx = JsonPath.parse(page);

        for (Map.Entry<T, _SelectorMetaInfo> selectorEntry : metaInfoMap.entrySet()) {
            T selector = selectorEntry.getKey();
            _SelectorMetaInfo metaInfo = selectorEntry.getValue();
            Object objValue = parse(pageCtx,metaInfo);
            results.put(selector, objValue);
        }
        return results;
    }

    public static <T extends Enum<T> & IEnumValue<_SelectorMetaInfo>>
    HashMap<T,_SelectorMetaInfo> getMap(Class<T> clazz)
    {
        HashMap<T,_SelectorMetaInfo> result = new HashMap<T,_SelectorMetaInfo>();
        for( T selector : clazz.getEnumConstants() ) {
            result.put( selector, selector.get() );
        }
        return result;
    }

    public static <T extends Enum<T> & IEnumValue<_SelectorMetaInfo>>
    Map<T,Object> parse( String pageStr, Class<T> clazz )
    {
        return parse(pageStr,getMap(clazz));
    }
}
