package com.piqube.marketplace.parser.parserutils;


import com.piqube.marketplace.parser.utils.UnaryFunction;

import java.util.Map;

public class _SelectorMetaInfo<T,V> {

    public static enum SelectorType {
        STRING,
        LIST,
        STRUCT
    };

    public static enum SelectorMode {
        TEXT,
        OWNTEXT,
        COMMENT,
        ATTR,
        ELEM
    };

    public String                             selector;
    public String                             attribute;
    public SelectorType                       selectorType;
    public SelectorMode                       selectorMode;
    public Map<T,? extends _SelectorMetaInfo> children;
    public UnaryFunction<T,V> postFunc;

    public _SelectorMetaInfo(String selector, String attribute, SelectorType selectorType,
                             SelectorMode selectorMode, UnaryFunction<T,V> postFunc) {
        this.selector     = selector;
        this.attribute    = attribute;
        this.selectorType = selectorType;
        this.selectorMode = selectorMode;
        this.postFunc     = postFunc;
    }

    private _SelectorMetaInfo() {
        this.selectorType = SelectorType.STRING;
        this.selectorMode = SelectorMode.TEXT;
    }

    public _SelectorMetaInfo(Arg... args) {
        this();
        for( Arg arg : args ) {
            switch(arg.argName) {
                case SELECTOR_TEXT:
                    this.selector = (String)arg.argValue;
                    continue;
                case SELECTOR_ATTR:
                    this.attribute    = (String)arg.argValue;
                    this.selectorMode = SelectorMode.ATTR;
                    continue;
                case SELECTOR_TYPE:
                    this.selectorType = (SelectorType)arg.argValue;
                    continue;
                case SELECTOR_MODE:
                    this.selectorMode = (SelectorMode)arg.argValue;
                    continue;
                case POST_FUNC:
                    this.postFunc = (UnaryFunction<T,V>)arg.argValue;
                    continue;
                case SELECTOR_CHILDREN:
                    if ( selectorType == SelectorType.STRING )
                        this.selectorType = SelectorType.STRUCT;
                    this.children = (Map<T,_SelectorMetaInfo>)arg.argValue;
                    continue;
            }
        }
    }
}
