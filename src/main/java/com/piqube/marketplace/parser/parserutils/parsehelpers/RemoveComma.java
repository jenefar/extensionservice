package com.piqube.marketplace.parser.parserutils.parsehelpers;

import com.piqube.marketplace.parser.utils.UnaryFunction;

/**
 * Created by lipika on 28/4/14.
 */
public class RemoveComma implements UnaryFunction<String,String> {

    public RemoveComma() {
    }

    public String exec(String arg) {
        return arg.replace(",", "");
    }
}
