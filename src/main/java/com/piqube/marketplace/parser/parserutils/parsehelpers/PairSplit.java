package com.piqube.marketplace.parser.parserutils.parsehelpers;

import com.piqube.marketplace.parser.utils.UnaryFunction;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONAware;
import net.minidev.json.JSONObject;

import java.util.Arrays;

/**
 * Created by ranjan on 22/4/14.
 */
// Split of pieces where first portion is considered a Name
public class PairSplit implements UnaryFunction<JSONAware,String> {

    public String delimiter    = ":";
    public String leftColName;
    public String rightColName;

    public static enum OutputType {
        Array,
        Map
    };

    public OutputType outputType = null;

    public PairSplit() {
    }

    public PairSplit(String delimiter) {
        this.delimiter = delimiter;
    }

    public PairSplit left(String leftColName) {
        this.leftColName = leftColName;
        return this;
    }

    public PairSplit right(String rightColName) {
        this.rightColName = rightColName;
        return this;
    }

    public PairSplit outputArray() {
        this.outputType = OutputType.Array;
        return this;
    }

    public PairSplit outputMap() {
        this.outputType = OutputType.Map;
        return this;
    }

    public JSONAware exec(String arg) {
        final String[] words = arg.split(delimiter,2);
        if ( this.outputType == OutputType.Array )
            return new JSONArray(){{ addAll(Arrays.asList(words)); }};
        else
            return new JSONObject(){{ put(PairSplit.this.leftColName,words[0]); put(PairSplit.this.rightColName, words[1]); }};
    }
}
