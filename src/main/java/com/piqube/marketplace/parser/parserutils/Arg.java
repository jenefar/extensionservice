package com.piqube.marketplace.parser.parserutils;

import com.piqube.marketplace.parser.IEnumValue;
import com.piqube.marketplace.parser.utils.UnaryFunction;

import java.util.Map;

public class Arg {

    public static enum ArgName {
        SELECTOR_TEXT,
        SELECTOR_ATTR,
        SELECTOR_MODE,
        SELECTOR_TYPE,
        SELECTOR_CHILDREN,
        POST_FUNC,
        POST_ELEM_FUNC
    };

    public ArgName argName;
    public Object  argValue;

    public Arg(ArgName argName, Object argValue) {
        this.argName = argName;
        this.argValue = argValue;
    }

    public static Arg E(String selectorText) {
        return new Arg(ArgName.SELECTOR_TEXT, selectorText);
    }

    public static Arg a(String selectAttr) {
        return new Arg(ArgName.SELECTOR_ATTR, selectAttr);
    }

    public static Arg ownText() {
        return new Arg(ArgName.SELECTOR_MODE, _SelectorMetaInfo.SelectorMode.OWNTEXT);
    }

    public static Arg comment() { return new Arg(ArgName.SELECTOR_MODE, _SelectorMetaInfo.SelectorMode.COMMENT); }

    public static <K,V> Arg F(UnaryFunction<K,V> postFunc) {
        return new Arg(ArgName.POST_FUNC, postFunc);
    }

    public static Arg elem() {
        return new Arg(ArgName.SELECTOR_MODE, _SelectorMetaInfo.SelectorMode.ELEM);
    }

    public static Arg list() {
        return new Arg(ArgName.SELECTOR_TYPE, _SelectorMetaInfo.SelectorType.LIST );
    }

    public static Arg Children(Map<String,_SelectorMetaInfo> children) {
        return new Arg(ArgName.SELECTOR_CHILDREN, children);
    }

    public static <T extends Enum<T> & IEnumValue> Arg Children(Class<T> clazz) {
        return new Arg(ArgName.SELECTOR_CHILDREN, HtmlParseUtils.getMap(clazz));
    }
}
