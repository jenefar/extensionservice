package com.piqube.marketplace.parser.parserutils;

import com.piqube.marketplace.parser.IEnumValue;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class HtmlParseUtils {

    final Log log = LogFactory.getLog(getClass());

    public static String elemToText(Element elem, String attr, _SelectorMetaInfo.SelectorMode selectorMode) {
        switch(selectorMode) {
            case TEXT:
                return elem.text();
            case ATTR:
                return elem.attr(attr);
            case OWNTEXT:
                return elem.ownText();
            case COMMENT:
                return elem.childNodes().get(0).attributes().get("comment");
            case ELEM:
                return elem.toString();
        }
        return null;
    }

    public static Object parseElement(Element element, _SelectorMetaInfo metaInfo) {
        String attrStr = metaInfo.attribute;
        _SelectorMetaInfo.SelectorMode selectorMode = metaInfo.selectorMode;
        if ( selectorMode != _SelectorMetaInfo.SelectorMode.ELEM ) {
            Object matchedRes = elemToText(element, attrStr, selectorMode);
            if (metaInfo.postFunc != null)
                matchedRes = metaInfo.postFunc.exec(matchedRes);
            return matchedRes;
        } else {
            String matchedStr;
            if (metaInfo.postFunc != null) {
                Object retVal = metaInfo.postFunc.exec(element);
                return retVal != null ? retVal.toString() : null;
            }
            else
                return element.toString();
        }
    }

    public static <T extends Enum<T> & IEnumValue<_SelectorMetaInfo>>
    HashMap<T,Object> parseStruct( Element element, _SelectorMetaInfo metaInfo)
    {
        HashMap<T,Object> matchedMap = new HashMap<>();
        for( Map.Entry<T,_SelectorMetaInfo> childMetaInfoEntry :
                ((Map<T,_SelectorMetaInfo>) metaInfo.children).entrySet() ) {
            T childSelectorName = childMetaInfoEntry.getKey();
            _SelectorMetaInfo childSelector = childMetaInfoEntry.getValue();
            Object childResult = parse( element, childSelector );
            if ( childResult != null )
                matchedMap.put( childSelectorName, childResult );
        }
        return matchedMap.isEmpty() ? null : matchedMap;
    }

    public static Object parse( Element summaryDoc , _SelectorMetaInfo metaInfo ) {
        String selectorStr = metaInfo.selector;
        _SelectorMetaInfo.SelectorType selectorType = metaInfo.selectorType;

        Elements elems = summaryDoc.select(selectorStr);
        if ( elems != null && elems.size() != 0 ) {
            Element firstMatchedElem;

            switch(selectorType) {
                case STRING: {
                    firstMatchedElem = elems.get(0);
                    return parseElement(firstMatchedElem, metaInfo);
                }
                case LIST: {
                    Set<Object> matchedSet = new LinkedHashSet<Object>();

                    for (Element matchedElem : elems) {
                        Object matchedObj = null;
                        if ( metaInfo.children == null || metaInfo.children.size() == 0 )
                            matchedObj = parseElement(matchedElem, metaInfo);
                        else
                            matchedObj = parseStruct( matchedElem, metaInfo );
                        if ( matchedObj != null )
                            matchedSet.add(matchedObj);
                    }
                    return matchedSet;
                }
                case STRUCT: {
                    return parseStruct(elems.get(0),metaInfo);
                }
            }
        }
        return null;
    }

    public static Object parse( String pageStr, _SelectorMetaInfo metaInfo ) {
        Document summaryDoc = Jsoup.parse(pageStr);
        return parse( summaryDoc, metaInfo );
    }

    public static <T extends Enum<T> & IEnumValue<_SelectorMetaInfo>>
    Map<T,Object> parse( String page, HashMap<T,_SelectorMetaInfo> metaInfoMap )
    {
        Document summaryDoc = Jsoup.parse(page);
        HashMap<T,Object> results = new HashMap<T, Object>();
        for( Map.Entry<T,_SelectorMetaInfo> selectorEntry : metaInfoMap.entrySet() ) {
            T selector = selectorEntry.getKey();
            _SelectorMetaInfo metaInfo = selectorEntry.getValue();
            Object result = parse( summaryDoc, metaInfo );
            if ( result != null )
                results.put( selector, result );
        }

        return results;
    }

    public static <T extends Enum<T> & IEnumValue<_SelectorMetaInfo>>
    HashMap<T,_SelectorMetaInfo> getMap(Class<T> clazz)
    {
        HashMap<T,_SelectorMetaInfo> result = new HashMap<T,_SelectorMetaInfo>();
        for( T selector : clazz.getEnumConstants() ) {
            result.put( selector, selector.get() );
        }
        return result;
    }

    public static <T extends Enum<T> & IEnumValue<_SelectorMetaInfo>>
    Map<T,Object> parse( String pageStr, Class<T> clazz )
    {
        return parse(pageStr,getMap(clazz));
    }
}