/**
 * Filename : LinkedInMemberIdFetcher
 * Description :
 * Date : 23 Jun, 2014
 * Owner : Clockwork Interviews Pvt., Ltd.,
 * Project : piqube-extensions
 * Contact : qube@piqube.com
 * History : 
 */
package com.piqube.marketplace.parser.parserutils.parsehelpers;

import com.piqube.marketplace.parser.utils.UnaryFunction;
import org.jsoup.nodes.Element;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author <a href="mailto:me@arulraj.net">Arul</a>
 */
public class LinkedInMemberIdFetcher implements UnaryFunction<String, Object> {
  @Override
  public String exec(Object input) {
    /**
     * TODO: have to make single Regex like newTrkInfo\s*=\s*'(\d*)\,'
     */
    String data = null;
    if(input instanceof Element) {
      data = ((Element) input).data();
    } else {
      data = input.toString();
    }
    if(data.contains("newTrkInfo = ")) {
      Pattern pattern = Pattern.compile("newTrkInfo =(.*)\\+");
      Matcher matcher = pattern.matcher(data);
      String retVal = null;
      if(matcher.find()) {
        retVal = matcher.group().trim();
        pattern = Pattern.compile("[0-9]\\d*");
        Matcher mm = pattern.matcher(retVal);
        if(mm.find()) {
          retVal = mm.group();
        }
      }
      return retVal;
    }
    return null;
  }
}
