package com.piqube.marketplace.parser;

/**
 * @author jenefar
 */
public enum CrawledSource {
    LINKEDIN,
    NAUKRI,
    INDEED,
    TECHGIG
}
