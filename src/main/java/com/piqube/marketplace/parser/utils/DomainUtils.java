package com.piqube.marketplace.parser.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.net.URI;
import java.net.URISyntaxException;

public class DomainUtils {

    final static Log log = LogFactory.getLog(DomainUtils.class);

    public static String getDomain(String uriStr) {
        try {
            URI uri = new URI(uriStr);
            return getDomain(uri);
        } catch(URISyntaxException ex) {
            log.error("Error in getDomain : "+ex.getMessage(),ex);
            return null;
        }
    }

    public static String getDomain(URI uri) {
        String host = uri.getHost();
        if ( host != null && host.startsWith("www.") )
            host = host.substring(4);
        return host;
    }
}
