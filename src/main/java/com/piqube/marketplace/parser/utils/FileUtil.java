package com.piqube.marketplace.parser.utils;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.math.BigInteger;
import java.net.URI;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Jenefar Pratap
 * Date: 1/5/13
 * Time: 11:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class FileUtil {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    /**
     * This method returns true if the collection is null or is empty.
     *
     * @param collection
     * @return true | false
     */
    public boolean isEmpty(Collection<?> collection) {
        if (collection == null || collection.isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * This method returns true of the map is null or is empty.
     *
     * @param map
     * @return true | false
     */
    public boolean isEmpty(Map<?, ?> map) {
        if (null == map || map.isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * This method returns true if the objet is null.
     *
     * @param object
     * @return true | false
     */
    public boolean isEmpty(Object object) {
        if (null == object) {
            return true;
        }
        return false;
    }

    /**
     * This method returns true if the input array is null or its length is zero.
     *
     * @param array
     * @return true | false
     */
    public boolean isEmpty(Object[] array) {
        if (array == null || array.length == 0) {
            return true;
        }
        return false;
    }

    /**
     * This method returns true if the input string is null or its length is zero.
     *
     * @param string
     * @return true | false
     */
    public boolean isEmpty(String string) {
        if (null == string || 0 == string.trim().length()) {
            return true;
        }
        if (string.contains("N/A")) {
            return true;
        }
        return false;
    }

    public boolean isFieldNotSpecified(String string) {
        if (string.contains("N/A")) {
            return true;
        }
        return false;
    }

    public Date dateFormatter(int year, int month, int day) {
        String date = year + "/" + month + "/" + day;
        Date utilDate = null;

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
            utilDate = formatter.parse(date);
            System.out.println("utilDate:" + utilDate);
        } catch (ParseException e) {
            //log.error(e.toString());
        }
        return utilDate;
    }

    public Date fullMonthDateFormat(String year, String month, String day) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy MMMMM dd");
        Date formattedDate = null;
        try {
            formattedDate = dateFormat.parse(year + " " + month + " " + day);
            LOGGER.debug("The generated date is :" + formattedDate);
        } catch (ParseException e) {
            LOGGER.debug("Date parse Exception.");
        }
        return formattedDate;
    }

    public String md5(String input) {

        String md5 = null;

        if (null == input) return null;

        try {

            //Create MessageDigest object for MD5
            MessageDigest digest = MessageDigest.getInstance("MD5");

            //Update input string in message digest
            digest.update(input.getBytes(), 0, input.length());

            //Converts message digest value in base 16 (hex)
            md5 = new BigInteger(1, digest.digest()).toString(16);

        } catch (NoSuchAlgorithmException e) {
            LOGGER.error(e.toString());
        }
        return md5;
    }

    public Date getDate(Calendar cal) {
        Date newDate = null;
        try {
            newDate = new SimpleDateFormat("yyyy-mm-dd").parse("" + cal.get(Calendar.YEAR) + "-" +
                    (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DATE));
        } catch (ParseException e) {
            LOGGER.error("Parse Exception" + e);
        }
        return newDate;
    }



    public void storeImage(String url, String path) {
    	FileOutputStream fos = null;
        try {
            URL fileUrl = new URL(url);
			ReadableByteChannel rbc = Channels.newChannel(fileUrl.openStream());
			fos = new FileOutputStream(path);
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        } catch (Exception e) {
            LOGGER.error("*** Error while Storing Image for url : " + url + " ended with exception ***" + e);
        } finally {
        	if(fos != null) {
        		try {
					fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	}
        }
    }

    public String createTextFile(String content, String path) {
        File file=null;
        try {
            file = new File(path);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            System.out.println("File path is:::"+file.getAbsolutePath());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(content);
            bw.close();
        } catch (IOException e) {
            LOGGER.error("*** Error while creating text file ended with exception ***" + e);
        }
       return file.getAbsolutePath();
    }

   public void createZipFile(String sourceFilePath, String destinationZipFilePath){
       byte[] buffer = new byte[1024];
       File newFile=new File(sourceFilePath);
       try {
           FileOutputStream fileOutputStream =new FileOutputStream(destinationZipFilePath);
           GZIPOutputStream zipOutputStream = new GZIPOutputStream(fileOutputStream);
           FileInputStream fileInput = new FileInputStream(sourceFilePath);
           int bytes_read;
           while ((bytes_read = fileInput.read(buffer)) > 0) {
               zipOutputStream.write(buffer, 0, bytes_read);
           }
           fileInput.close();
           zipOutputStream.finish();
           zipOutputStream.close();
           newFile.delete();
           LOGGER.debug("The file Name : "+newFile.getName()+" compressed successfully!");
       } catch (IOException ex) {
           ex.printStackTrace();
       }

   }
    public void unzipFile(String sourceFilePath,String destinationFilePath){
        try {
        File newFile=new File(sourceFilePath);
        GZIPInputStream in = new GZIPInputStream(new FileInputStream(sourceFilePath));
        FileOutputStream fileOutputStream=new FileOutputStream(destinationFilePath);
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            fileOutputStream.write(buf, 0, len);
        }
        in.close();
        LOGGER.debug("The file Name : "+newFile.getName()+" deCompressed successfully!");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public  String requestUrl(URI uri) throws IOException {


        StringWriter writer = null;
        try {
            HttpClient httpclient = HttpClients.createSystem();
            HttpGet request = new HttpGet(uri);
            RequestConfig config = RequestConfig.custom()
                    .build();

            request.setConfig(config);
            System.out.println("Request sent to :" + uri.toString());
            HttpResponse response = httpclient.execute(request);
            InputStream inputStream = response.getEntity().getContent();

            writer = new StringWriter();
            IOUtils.copy(inputStream, writer, "UTF-8");
        }

        catch (Exception e)
        {
            e.printStackTrace();
        }


        if(null!=writer)
            return writer.toString();
        else
            return null;
    }
}
