package com.piqube.marketplace.parser.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ObjectUtils {

    final Log log = LogFactory.getLog(getClass());

    public static boolean equals(String a, String b) {
        return (a == null) ? (a == b) : a.equals(b);
    }

    public static <T> boolean equals(Set<T> a, Set<T> b) {
        return (a == null) ? (a == b) : a.equals(b);
    }

    public static <T> boolean equals(List<T> a, List<T> b) {
        return (a == null) ? (a == b) : a.equals(b);
    }

    public static <K, V> boolean equals(Map<K, V> a, Map<K, V> b) {
        return (a == null) ? (a == b) : a.equals(b);
    }

    public static String md5(String input) {

        if (null == input) return null;

        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        digest.update(input.getBytes(), 0, input.length());
        String md5 = new BigInteger(1, digest.digest()).toString(16);

        return md5;
    }

    public static String cleanUp(String input) {
        if ( input == null ) return null;
        return input.replaceAll("\u00a0", " ").replaceAll("[\\s]+", " ").trim();
    }
}
